granite
august
black


High level mechanics - player launches these at NPCs
> Greet
> Interrogate

Entity
Name: Blah blah
Position: 0,0
Dialogue Tone: GENERIC

Line type: PROMPT or RESPONSE
Prereq tags: <HAS_INTEL> <NO_INTEL>
Effect tags: (OUT_INTEL)

BRANCH Greet

PROMPT Hello.
PROMPT Hey.
PROMPT What's up.


RESPONSE Greetins.
RESPONSE Howdy.
RESPONSE What?
RESPONSE What do you want?
RESPONSE Yeah, what?
RESPONSE Get to the point.
RESPONSE What now?
RESPONSE <HAS_INTEL> (OUT_INTEL) Hey buddy. I heard something interesting.



BRANCH Interrogate

PROMPT Spit it out.
PROMPT Give me the intel.
PROMPT What can you tell me?
PROMPT Talk.

RESPONSE <HAS_INTEL> (OUT_INTEL) Mornin.
RESPONSE <NO_INTEL> I don't know anything!
RESPONSE <NO_INTEL> I don't know anything you don't.
RESPONSE <NO_INTEL> You already know everything.



BRANCH Ask_Advice

PROMPT Any tips?
PROMPT What should I do?

RESPONSE 




1 Agent %name, welcome to the SAD family.
2 Your first mission is already on standby. Head to the elevator when you're ready.
3 Don't forget to pick up your equipment at the armory.




You look a little green around the gills... the battlefield is a dangerous place. 
Talk to the training officers to get yourself up to speed.



> Training
    You should talk to Manny, learn hand-to-hand fighting techniques.
    The enemy might surprise you. Or maybe you want to surprise them.
    Either way, confidence in close quarters is critical.








- Armorer

Welcome to the armory, Agent.
As you know, SAD missions are absolute top-secret. 
You can't take anything into the field that would reveal your true origin.
Weapons and equipment are on-sight-procurement. OSP. It's how we work.
But we can still give you some generic tools.



TRAINING

MANNY - CQC mechanics, close combat

GUNNY - Guns and shooting mechanics



