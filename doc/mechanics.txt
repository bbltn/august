punch - default (+ from karate)
push - default (+ from karate)
grab - default (+ from ju jitsu)
choke - default (+ from ju jitsu)
throw - judo
disarm - judo
takedown - ju jitsu
sweep - ju jitsu
resuscitate - default (+ from combat medicine)
healing - combat medicine


Stumbling and Dodging
 - When an entity makes a touch attack, it triggers the chance for the target to dodge.
 - If a target dodges a melee attack, the attack fails and the attacker stumbles.
 - Target must be aware of attacker to dodge.
 - If the dodge fails, the target has a chance to block.
 - Blocked attacks do minimum KO damage, and never cause KO.
 - If the target does not dodge, and the touch attack misses, it triggers the chcance for the target to stumble.

Healing
 - KO'd entities can be resuscited up to half KO
 - hurt entities can be healed up to half health
 - drugs can be used to recover KO or health, but with chance of overdose
	 - first overdose causes half the normal effect; deals damage each time after