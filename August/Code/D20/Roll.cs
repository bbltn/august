﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public interface IDie
    {
        int Range { get; }
        int Result { get; }
        bool Success(int dc);
        int Difference(int dc);
        bool NaturalCrit { get; }
        bool NaturalFail { get; }
    }

    public class Die
    {
        public int Range { get; private set; }
        public int Result { get; private set; }

        public Die(int range )
        {
            Range = range;
            Result = new Random(Guid.NewGuid().GetHashCode()).Next(Range) + 1;
        }

        public bool Succeeds(int dc = 10)
        {
            return Result >= dc;
        }

        public int Difference(int dc = 10)
        {
            return Result - dc;
        }

        public bool NaturalCrit
        {
            get
            {
                return Result == 20;
            }
        }
        public bool NaturalFail
        {
            get
            {
                return Result == 0;
            }
        }
    }
}
