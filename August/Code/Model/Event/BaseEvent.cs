﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public enum ReturnCode { Done, NotDone, Cancel }
    public enum EventOccurrence { Immediate, Delayed }

    public interface IGameEvent
    {
        void Process();
        ReturnCode State { get; }
        IEventInfo Info { get; }
        EventOccurrence Occurrence { get; }
    }
    
    public class BaseEvent : IGameEvent
    {
        public ReturnCode State { get; protected set; }
        public IEventInfo Info { get; protected set; }
        public EventOccurrence Occurrence { get; protected set; }

        private bool askedForEntity { get; set; }

        public BaseEvent(IEventInfo info)
        {
            Info = info;
            State = ReturnCode.NotDone;
            Occurrence = EventOccurrence.Immediate;

            askedForEntity = false;
        }

        public virtual void Process()
        {
            State = ReturnCode.Done;
        }

        protected void SetCostByAptitude(string skill, int untrained = 3, int trained = 2, int expert = 1)
        {
            switch (Info.ActiveEntity.skills.CheckAptitude(skill))
            {
                case AptitudeLevel.Expert:
                    Info.Cost = expert;
                    break;
                case AptitudeLevel.Trained:
                    Info.Cost = trained;
                    break;
                default:
                    Info.Cost = untrained;
                    break;
            }
            return;
        }

        protected void AcquireTargetEntity()
        {
            VectorCommand vectorCommand;

            if (Info.hasTargetEntity)
                return;

            var adjacents = Info.ActiveEntity.knowledge.GetAdjacent();
            if (adjacents.Count == 1)
            {
                Info.TargetEntity = adjacents[0];
                State = ReturnCode.NotDone;
                return;
            }

            if (!askedForEntity)
            {
                Info.model.Messages.Add("Select a target.");
                askedForEntity = true;
                State = ReturnCode.NotDone;
                return;
            }

            if (Info.model.Commands.Count == 0)
            {
                State = ReturnCode.NotDone;
                return;
            }

            vectorCommand = Info.model.Commands[0] as VectorCommand;
            Info.model.Commands.Clear();

            if (vectorCommand == null)
            {
                State = ReturnCode.NotDone;
                return;
            }

            if (vectorCommand != null)
            {
                var tile = Info.model.Scene.GetTile(Info.ActiveEntity.tile.Position + vectorCommand.Vector);
                if (tile != null)
                    Info.TargetEntity = tile.Entity;
            }

            if (!Info.hasTargetEntity)
            {
                Info.model.Messages.Add("Okay, then.");
                State = ReturnCode.Cancel;
                return;
            }            

            return;
        }
    }
}
