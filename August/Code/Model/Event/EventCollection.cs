﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public interface IEventCollection
    {
        void Update();
        void Add(IGameEvent newEvent);
        int Count { get; }
        bool IsResolved { get; }
        void Reset();
        bool Empty { get; }
    }


    public class EventCollection : IEventCollection
    {
        private const bool PrintDebugMessages = false;
        private List<IGameEvent> Events;
        public int Count { get { return Events.Count; } }
        public bool IsResolved 
        { 
            get 
            {
                if (Empty)
                    return true;

                foreach (var ev in Events)
                {
                    if (HasReachedCompletion(ev) && ev.State == ReturnCode.NotDone)
                        return false;

                    if (ev.Occurrence == EventOccurrence.Immediate && ev.State == ReturnCode.NotDone)
                        return false;
                }

                return true;
            } 
        }

        private bool HasReachedCompletion(IGameEvent ev)
        {
            return (ev.Info.model.Scene.Time.Current >= ev.Info.StartTime + ev.Info.Cost);
        }


        public bool Empty { get { return (Events.Count <= 0); } }

        public EventCollection()
        {
            Reset();
        }

        public void Reset()
        {
            Events = new List<IGameEvent>();
        }

        public void Update()
        {
            if (Empty) return;

            List<IGameEvent> finishedEvents = new List<IGameEvent>();

            for (int iter = 0; iter < Events.Count; iter++)
            {
                var curEvent = Events[iter];

                if (curEvent.State == ReturnCode.Cancel)
                    finishedEvents.Add(curEvent);

                else if (curEvent.State == ReturnCode.Done)
                {
                    if (HasReachedCompletion(curEvent))
                        finishedEvents.Add(curEvent);
                }

                else
                {
                    if (curEvent.Occurrence == EventOccurrence.Immediate)
                        curEvent.Process();
                    else if (HasReachedCompletion(curEvent))
                        curEvent.Process();
                }
            }

            foreach (var ev in finishedEvents)
            {
                Events.Remove(ev);
                if (ev.Info.ActiveEntity.curEvent == ev)
                    ev.Info.ActiveEntity.curEvent = null;

                if (PrintDebugMessages)
                    ev.Info.model.Messages.Add(new Malison.Core.CharacterString(TimeUtils.UnitsToString(ev.Info.model.Scene.Time.Current) + " " + ev.Info.ActiveEntity.BaseType + " " + ev.ToString() + " completed", Malison.Core.TermColor.DarkGreen, Malison.Core.TermColor.Black));
            }
        }

        public void Add(IGameEvent newEvent)
        {
            List<IGameEvent>ClearList = new List<IGameEvent>();
            foreach (var ev in Events)
            {
                if (ev.Info.ActiveEntity == newEvent.Info.ActiveEntity)
                    ClearList.Add(ev);
            }

            foreach (var thing in ClearList)
            {
                if (PrintDebugMessages)
                    newEvent.Info.model.Messages.Add(new Malison.Core.CharacterString(TimeUtils.UnitsToString(thing.Info.model.Scene.Time.Current) + " " + thing.Info.ActiveEntity.BaseType + " " + thing.ToString() + " cleared", Malison.Core.TermColor.Gray, Malison.Core.TermColor.Black));
                Events.Remove(thing);
            }

            Events.Add(newEvent);
            newEvent.Info.ActiveEntity.curEvent = newEvent;

            if (PrintDebugMessages)
                newEvent.Info.model.Messages.Add(new Malison.Core.CharacterString(TimeUtils.UnitsToString(newEvent.Info.model.Scene.Time.Current) + " " + newEvent.Info.ActiveEntity.BaseType + " " + newEvent.ToString() + " added", Malison.Core.TermColor.DarkCyan, Malison.Core.TermColor.Black));
        }
    }
}
