﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    class FallEvent : BaseEvent
    {
        public FallEvent(IEventInfo info)
            : base(info)
        {
            Occurrence = EventOccurrence.Immediate;
        }

        public override void Process()
        {
            if (Info.ActiveEntity == null)
            {
                State = ReturnCode.Cancel;
                return;
            }

            switch (Info.ActiveEntity.stance.current)
            {
                case StanceType.PRONE:
                    State = ReturnCode.Cancel;
                    return;

                case StanceType.CROUCH:
                    Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
                    Info.Cost = 1;
                    State = ReturnCode.Done;
                    break;

                case StanceType.STAND:
                    Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
                    Info.Cost = 2;
                    State = ReturnCode.Done;
                    break;

                default:
                    Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
                    Info.Cost = 1;
                    State = ReturnCode.Done;
                    break;
            }

            if (Info.Verbose)
                Info.model.Messages.Add(GetDescription(Info));

            return;
        }

        private static string GetDescription(IEventInfo Info)
        {
            string valueString;
            switch (new Die(3).Result)
            {
                case 1:
                    valueString = Info.ActiveEntity.BaseType + " " + GetPastTenseVerb() +  " to prone.";
                    break;

                case 2:
                    if (Info.ActiveEntity.tile.hasFeature)
                        valueString = Info.ActiveEntity.BaseType + " " + GetPastTenseVerb() + " to the " + Info.ActiveEntity.tile.Feature.Name + ".";
                    else
                        valueString = Info.ActiveEntity.BaseType + " " + GetPastTenseVerb() + " to thee ground.";
                    break;

                default:
                    valueString = Info.ActiveEntity.BaseType + " " + GetPastTenseVerb() + ".";
                    break;
            }
            return valueString;
        }

        private static string GetPastTenseVerb()
        {
            string valueString;
            switch (new Die(6).Result)
            {
                case 1:
                    valueString = "fell";
                    break;

                case 2:
                    valueString = "collapsed";
                    break;

                case 3:
                    valueString = "dropped";
                    break;

                case 4:
                    valueString = "crumpled";
                    break;

                case 5:
                    valueString = "folded";
                    break;

                default:
                    valueString = "fainted";
                    break;
            }
            return valueString;
        }
    }
}
