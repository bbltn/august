﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    class ProneEvent : BaseEvent
    {
        public ProneEvent(IEventInfo info)
            : base(info)
        {
            Occurrence = EventOccurrence.Delayed;
        }

        public override void Process()
        {
            if (Info.ActiveEntity == null)
            {
                State = ReturnCode.Cancel;
                return;
            }

            switch (Info.ActiveEntity.stance.current)
            {
                case StanceType.PRONE:
                    State = ReturnCode.Cancel;
                    break;

                case StanceType.CROUCH:
                    Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
                    Info.Cost = 1;
                    State = ReturnCode.Done;
                    break;

                case StanceType.STAND:
                    Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
                    Info.Cost = 2;
                    State = ReturnCode.Done;
                    break;

                default:
                    Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
                    Info.Cost = 1;
                    State = ReturnCode.Done;
                    break;
            }

            if (Info.Verbose)
                Info.model.Messages.Add(Info.ActiveEntity.BaseType + " lay down prone.");

            return;
        }
    }
}
