﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    class CrouchEvent : BaseEvent
    {
        public CrouchEvent(IEventInfo info)
            : base(info)
        {
            Occurrence = EventOccurrence.Delayed;
        }

        public override void Process()
        {
            if (Info.ActiveEntity == null)
            {
                State = ReturnCode.Cancel;
                return;
            }

            switch (Info.ActiveEntity.stance.current)
            {
                case StanceType.CROUCH:
                    State = ReturnCode.Cancel;
                    break;

                default:
                    Info.ActiveEntity.stance.SetStance(StanceType.CROUCH);
                    Info.Cost = 1;
                    State = ReturnCode.Done;
                    break;
            }

            if (Info.Verbose)
                Info.model.Messages.Add(Info.ActiveEntity.BaseType + " crouched.");

            return;
        }
    }
}
