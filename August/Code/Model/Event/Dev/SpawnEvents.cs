﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class ForceSpawnEvent : BaseEvent
    {
        public ForceSpawnEvent(IEventInfo info)
            : base(info)
        {
        }

        public override void Process()
        {
            IEntity entity = new BaseEntity(Info.model.Scene);
            Vec position = Info.Vector;
            Info.model.Scene.AddEntity(entity, position);
            

            State = ReturnCode.Done;
            return;
        }
    }

    class ForceDespawnEvent : BaseEvent
    {
        public ForceDespawnEvent(IEventInfo info)
            : base(info)
        {
        }

        public override void Process()
        {
            Vec position = Info.Vector;
            Info.model.Scene.RemoveEntity(position);            

            State = ReturnCode.Done;
            return;
        }

    }
}
