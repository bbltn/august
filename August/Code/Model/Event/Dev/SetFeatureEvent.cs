﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class SetFeatureEvent : BaseEvent
    {
        public SetFeatureEvent(IEventInfo info)
            : base(info)
        {
        }


        public override void Process()
        {
            var tile = Info.model.Scene.GetTile(Info.Vector);
            if (tile == null)
            {
                State = ReturnCode.Cancel;
                return;
            }

            tile.SetFeature(FileOperations.CharToFeature(Info.Text[0]));

            State = ReturnCode.Done;
            return;
        }
    }
}
