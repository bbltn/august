﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class LaunchEvent : BaseEvent
    {
        public LaunchEvent(IEventInfo info)
            : base(info)
        {
            info.Cost = 1;
        }

        public override void Process()
        {
            if (Info.Vector == Vec.Zero || !Info.hasActiveEntity)
            {
                State = ReturnCode.Cancel;
                return;
            }

            // if, like icarus, we have flown too far
            if (Info.Counter <= 0)
            {
                GroundEntity();
                State = ReturnCode.Done;
                return;
            }

            //Info.Counter--;

            // if we're about to fly out of the map...
            if (!Info.model.Scene.ContainsPosition(GetTargetPosition()))
            {
                GroundEntity();
                State = ReturnCode.Done;
                return;
            }

            if (Info.ActiveEntity.stance.current != StanceType.STAND)
                Info.ActiveEntity.stance.SetStance(StanceType.STAND);

            Tile targetTile = GetTargetTile();

            if (targetTile == null)
            {
                GroundEntity();
                State = ReturnCode.Done;
                return;
            }

            if (targetTile.consumesObjects)
            {
                ConsumeEntity(targetTile);
                State = ReturnCode.Done;
                return;
            }

            if (targetTile.hasEntity)
            {
                //GroundEntity();
                HitEntity(targetTile.Entity);
                State = ReturnCode.Done;
                return;
            }

            if (targetTile.hasFeature)
            {

                if (targetTile.Feature.isBreakable)
                {
                    if (Info.Verbose)
                        Info.model.Messages.Add(Info.ActiveEntity.BaseType + " crashes through the " + targetTile.Feature.Name + ".");
                    targetTile.BreakFeature();
                }

                if (targetTile.Feature.blocksMovement)
                {
                    HitFeature(targetTile.Feature);
                    State = ReturnCode.Done;
                    return;
                }
            }

            MoveEntity(targetTile);
            State = ReturnCode.Done;
            
            var newInfo = new BaseEventInfo(Info.model);
            newInfo.ActiveEntity = Info.ActiveEntity;
            newInfo.Counter = Info.Counter - 1;
            newInfo.Vector = Info.Vector;
            Info.model.Events.Add(new LaunchEvent(newInfo));

            return;
        }

        private void ConsumeEntity(Tile targetTile)
        {
            Info.model.Scene.RemoveEntity(Info.ActiveEntity.tile.Position);
            Info.model.Messages.Add(Info.ActiveEntity.BaseType + " disappeared into the " + targetTile.Feature.Name + "."); 
        }

        private void GroundEntity()
        {
            Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
            bool awakeBeforeGrounding = Info.ActiveEntity.health.Awake;
            Info.ActiveEntity.health.ApplyKnockoutDamage(new Die(3).Result);

            if (Info.Verbose)
            {
                Info.model.Messages.Add(Info.ActiveEntity.BaseType + GetGroundingDescriptor() + " the " + Info.ActiveEntity.tile.Surface + ".");

                if (awakeBeforeGrounding && !Info.ActiveEntity.health.Awake)
                    Info.model.Messages.Add(Info.ActiveEntity.BaseType + " passed out.");
            }
        }

        private void HitFeature(IFeature other)
        {
            Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
            
            bool awakeBeforeGrounding = Info.ActiveEntity.health.Awake;
            Info.ActiveEntity.health.ApplyKnockoutDamage(new Die(3).Result);
            
            if (Info.Verbose)
            {
                Info.model.Messages.Add(Info.ActiveEntity.BaseType + GetGroundingDescriptor() + "the " + other.Name + ".");
                if (awakeBeforeGrounding && !Info.ActiveEntity.health.Awake)
                    Info.model.Messages.Add(Info.ActiveEntity.BaseType + " passed out.");
            }
        }

        private void HitEntity(IEntity other)
        {
            Info.ActiveEntity.stance.SetStance(StanceType.PRONE);
            
            // should do this with an event so as to impose time costs
            other.stance.SetStance(StanceType.PRONE);
            
            bool awakeBeforeGrounding = Info.ActiveEntity.health.Awake;
            bool otherAwakeBeforeGrounding = other.health.Awake;
            Info.ActiveEntity.health.ApplyKnockoutDamage(new Die(3).Result);
            other.health.ApplyKnockoutDamage(new Die(3).Result);


            if (Info.Verbose)
            {
                Info.model.Messages.Add(Info.ActiveEntity.BaseType + GetGroundingDescriptor() + other.BaseType + " and fell over.");
                if (awakeBeforeGrounding && !Info.ActiveEntity.health.Awake)
                    Info.model.Messages.Add(Info.ActiveEntity.BaseType + " passed out.");

                if (otherAwakeBeforeGrounding && !other.health.Awake)
                    Info.model.Messages.Add(other.BaseType + " passed out.");
            }
            
        }

        private string GetGroundingDescriptor()
        {
            switch (new Die(4).Result)
            {
                case 1:
                    return " smacked into ";

                case 2:
                    return " hit ";

                case 3:
                    return " crumpled into ";

                default:
                    return " slammed into ";
            }
        }

        private void MoveEntity(Tile targetTile)
        {
            var leavingTile = Info.ActiveEntity.tile;

            leavingTile.SetEntity(null);
            targetTile.SetEntity(Info.ActiveEntity);
        }

        private Vec GetTargetPosition()
        {
            return Info.ActiveEntity.tile.Position + Info.Vector;
        }

        private Tile GetTargetTile()
        {
            return Info.model.Scene.GetTile(GetTargetPosition());
        }
    }
}
