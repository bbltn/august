﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class WaitEvent : BaseEvent
    {
        public WaitEvent(IEventInfo info)
            : base(info)
        {
            Info.Cost = 1;
        }

        public override void Process()
        {
            bool nearKO = Info.ActiveEntity.health.curKO == 1;
            if (nearKO)
            {
                int dc = 15;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Combat Medicine") >= (int)AptitudeLevel.Trained)
                    dc -= 2;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Combat Medicine") >= (int)AptitudeLevel.Expert)
                    dc -= 3;
                if (Info.ActiveEntity.health.curHP < (Info.ActiveEntity.health.maxHP / 2))
                    dc += 5;

                bool success = (new Die(20).Result > dc);

                if (success && Info.hasActiveEntity && Info.ActiveEntity.health.Awake && Info.ActiveEntity.health.Alive)
                {
                    Info.ActiveEntity.health.HealKnockoutDamage(1);
                }
            }

            State = ReturnCode.Done;
            return;
        }
    }
}
