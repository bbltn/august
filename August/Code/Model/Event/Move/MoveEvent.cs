﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class MoveEvent : BaseEvent
    {
        public MoveEvent(IEventInfo info)
            : base(info)
        {
        }

        public override void Process()
        {
            if (HasBadInfo())
            {
                State = ReturnCode.Cancel;
                return;
            }

            /*
            if (Info.ActiveEntity.Energy < 100)
            {
                Info.model.Messages.Add("why the fuck is this getting called");
            }
             */

            if (!Info.ActiveEntity.health.Alive || !Info.ActiveEntity.health.Awake)
            {
                State = ReturnCode.Cancel;
                return;
            }

            if (!Info.model.Scene.ContainsPosition(GetTargetPosition()))
            {
                State = ReturnCode.Cancel;
                return;
            }

            Tile targetTile = GetTargetTile();

            if (targetTile == null)
            {
                State = ReturnCode.Cancel;
                return;
            }

            if (targetTile.hasEntity)
            {
                State = ReturnCode.Done;
                return;
            }

            if (targetTile.isBlocked)
            {
                State = ReturnCode.Cancel;
                return;
            }

            ModifyCostByStance();
            MoveEntity(targetTile); 
            State = ReturnCode.Done;

            return;
        }

        private void ModifyCostByStance()
        {
            switch (Info.ActiveEntity.stance.current)
            {
                case StanceType.CROUCH:
                    Info.Cost += 5;
                    break;

                case StanceType.PRONE:
                    Info.Cost += 10;
                    break;

                default:
                    break;
            }
        }

        private void MoveEntity(Tile targetTile)
        {
            var leavingTile = Info.ActiveEntity.tile;

            leavingTile.SetEntity(null);
            targetTile.SetEntity(Info.ActiveEntity);
        }

        private Vec GetTargetPosition()
        {
            return Info.ActiveEntity.tile.Position + Info.Vector;
        }

        private Tile GetTargetTile()
        {
            return Info.model.Scene.GetTile(GetTargetPosition());
        }

        private bool HasBadInfo()
        {
            if (HasZeroVector())
                return true;
            if (HasNullEntity())
                return true;

            return false;
        }

        private bool HasZeroVector()
        {
            if (Info.Vector == Vec.Zero)
                return true;
            return false;
        }

        private bool HasNullEntity()
        {
            if (!Info.hasActiveEntity)
                return true;
            return false;
        }
    }
}
