﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class StumbleEvent : BaseEvent
    {
        public StumbleEvent(IEventInfo info)
            : base(info)
        {
            Info.Cost = 0;
        }

        private bool hasBadInfo
        {
            get
            {
                if (!Info.hasActiveEntity)
                    return true;
                if (!Info.ActiveEntity.health.Alive)
                    return true;
                if (!Info.ActiveEntity.health.Awake)
                    return true;
                return false;
            }
        }

        public override void Process()
        {
            if (hasBadInfo)
            {
                State = ReturnCode.Cancel;
                return;
            }

            var adjacentTiles = Info.model.Scene.GetAdjacentUnblockedTiles(Info.ActiveEntity.tile.Position);

            if (adjacentTiles.Count == 0)
            {
                if (Info.ActiveEntity.stance.current != StanceType.PRONE)
                {
                    BaseEventInfo fallInfo = new BaseEventInfo(Info.model);
                    fallInfo.ActiveEntity = Info.ActiveEntity;
                    Info.model.Events.Add(new FallEvent(fallInfo));
                }

                else if (Info.Verbose)
                        Info.model.Messages.Add(GetWritheDescription());
                
                State = ReturnCode.Done;
                return;
            }

            var rng = new Random();
            int index = rng.Next(0, adjacentTiles.Count);
            Info.Vector = adjacentTiles[index].Position - Info.ActiveEntity.tile.Position;


            if (Info.Verbose)
            {
                Info.model.Messages.Add(GetStumbleDescription());
                if (adjacentTiles[index].consumesObjects)
                    Info.model.Messages.Add(Info.ActiveEntity.BaseType + " nearly fell in the " + adjacentTiles[index].Feature.Name + ".");
            }

            var newInfo = new BaseEventInfo(Info.model);
            newInfo.ActiveEntity = Info.ActiveEntity;
            newInfo.Vector = Info.Vector;
            Info.model.Events.Add(new MoveEvent(newInfo));
            newInfo.Cost = 2;

            State = ReturnCode.Done;
            return;
        }

        private string GetStumbleDescription()
        {
            List<string> options = new List<string>();

            options.Add(Info.ActiveEntity.BaseType + " stumbled.");
            options.Add(Info.ActiveEntity.BaseType + " lurched away.");
            options.Add(Info.ActiveEntity.BaseType + " reeled.");
            options.Add(Info.ActiveEntity.BaseType + " lost his balance.");

            return options[new Die(options.Count).Result - 1];
        }

        private string GetWritheDescription()
        {
            return Info.ActiveEntity.BaseType + " writhed.";
        }
    }
}
