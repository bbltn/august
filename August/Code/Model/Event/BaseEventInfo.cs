﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public interface IEventInfo
    {
        Model model { get; }
        
        IEntity ActiveEntity { get; set; }
        IEntity TargetEntity { get; set; }

        bool hasActiveEntity { get; }
        bool hasTargetEntity { get; }
        
        Vec Vector { get; set; }

        int Counter { get; set; }

        string Text { get; set; }

        bool Verbose { get; set; }

        int StartTime { get; }

        int Cost { get; set;  }
    }

    public class BaseEventInfo : IEventInfo
    {
        public Model model { get; private set; }

        public IEntity ActiveEntity { get; set; }
        public IEntity TargetEntity { get; set; }

        public bool hasActiveEntity { get { return (ActiveEntity != null); } }
        public bool hasTargetEntity { get { return (TargetEntity != null); } }

        public Vec Vector { get; set; }
        public string Text { get; set; }

        public bool Verbose { get; set; }

        public int Counter { get; set; }

        public int StartTime { get; protected set; }

        public int Cost { get; set; }

        public BaseEventInfo(Model newModel)
        {
            model = newModel;
            Verbose = true;
            StartTime = model.Scene.Time.Current;
            Cost = 2;
        }
    }
}
