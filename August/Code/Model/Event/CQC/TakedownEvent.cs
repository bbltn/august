﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class TakedownEvent : BaseEvent
    {
        public bool askedForEntity = false;
        public bool askedForDirection = false;

        public TakedownEvent(IEventInfo info)
            : base(info)
        {
        }

        public override void Process()
        {
            VectorCommand vectorCommand;

            // find out who to take down
            if (!Info.hasTargetEntity)
            {
                var adjacents = Info.ActiveEntity.knowledge.GetAdjacentEnemies();
                if (adjacents.Count == 1)
                {
                    Info.TargetEntity = adjacents[0];
                    State = ReturnCode.NotDone;
                    return;
                }

                if (!askedForEntity)
                {
                    Info.model.Messages.Add("Select a target to take down.");
                    askedForEntity = true;
                    State = ReturnCode.NotDone;
                    return;
                }

                if (Info.model.Commands.Count == 0)
                {
                    State = ReturnCode.NotDone;
                    return;
                }

                vectorCommand = Info.model.Commands[0] as VectorCommand;
                Info.model.Commands.Clear();

                if (vectorCommand == null)
                {
                    State = ReturnCode.NotDone;
                    return;
                }

                if (vectorCommand != null)
                {
                    var tile = Info.model.Scene.GetTile(Info.ActiveEntity.tile.Position + vectorCommand.Vector);
                    if (tile != null)
                        Info.TargetEntity = tile.Entity;
                }

                if (!Info.hasTargetEntity)
                {
                    Info.model.Messages.Add("Okay, then.");
                    State = ReturnCode.Cancel;
                    return;
                }
            }

            vectorCommand = null;

            // find out where to take them down
            if (Info.Vector == Vec.Zero)
            {
                if (!askedForDirection)
                {
                    Info.model.Messages.Add("Select a direction.");
                    askedForDirection = true;
                    State = ReturnCode.NotDone;
                    return;
                }

                vectorCommand = Info.model.Commands[0] as VectorCommand;
                Info.model.Commands.Clear();

                if (vectorCommand == null)
                {
                    State = ReturnCode.NotDone;
                    return;
                }

                if (vectorCommand != null)
                {
                    var tile = Info.model.Scene.GetTile(Info.ActiveEntity.tile.Position + vectorCommand.Vector);
                    if (tile != null)
                    {
                        if (!tile.isBlocked && !tile.hasEntity)
                        Info.Vector = vectorCommand.Vector;
                    }
                }

                if (Info.Vector == Vec.Zero)
                {
                    Info.model.Messages.Add("Tile not clear.");
                    State = ReturnCode.Cancel;
                    return;
                }
            }

            // get the target tile
            Tile targetTile = Info.model.Scene.GetTile(Info.ActiveEntity.tile.Position + Info.Vector);
            if (targetTile == null)
            {
                State = ReturnCode.Cancel;
                return;
            }
            if (targetTile.hasEntity)
            {
                State = ReturnCode.Done;
                return;
            }
            if (targetTile.isBlocked)
            {
                State = ReturnCode.Cancel;
                return;
            }

            // perform the move
            var leavingTile = Info.TargetEntity.tile;
            leavingTile.SetEntity(null);
            targetTile.SetEntity(Info.TargetEntity);

            // apply the effects
            var awakeBeforeThrow = Info.TargetEntity.health.Awake;
            
            var force = new Die(3).Result;
            Info.TargetEntity.health.ApplyKnockoutDamage(force);

            Info.TargetEntity.stance.SetStance(StanceType.PRONE);

            if (Info.Verbose)
            {
                string surface;

                if (targetTile.hasFeature)
                    surface = targetTile.Feature.Name;
                else surface = "ground";

                Info.model.Messages.Add(Info.ActiveEntity.BaseType + " threw " + Info.TargetEntity.BaseType + " to the " + surface + ".");
            }

            if (!Info.TargetEntity.health.Awake && awakeBeforeThrow && Info.Verbose)
                Info.model.Messages.Add(Info.TargetEntity.BaseType + " went unconscious.");
        }
    }
}
