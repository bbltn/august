﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class SweepEvent : BaseEvent
    {
        public bool askedForEntity = false;

        public SweepEvent(IEventInfo info)
            : base(info)
        {
            Occurrence = EventOccurrence.Delayed;
        }

        public override void Process()
        {
            VectorCommand vectorCommand;

            // find out who to sweep
            if (!Info.hasTargetEntity)
            {
                var adjacents = Info.ActiveEntity.knowledge.GetAdjacentEnemies();
                if (adjacents.Count == 1)
                {
                    Info.TargetEntity = adjacents[0];
                    State = ReturnCode.NotDone;
                    return;
                }

                if (!askedForEntity)
                {
                    Info.model.Messages.Add("Select a target to sweep.");
                    askedForEntity = true;
                    State = ReturnCode.NotDone;
                    return;
                }

                if (Info.model.Commands.Count == 0)
                {
                    State = ReturnCode.NotDone;
                    return;
                }

                vectorCommand = Info.model.Commands[0] as VectorCommand;
                Info.model.Commands.Clear();

                if (vectorCommand == null)
                {
                    State = ReturnCode.NotDone;
                    return;
                }

                if (vectorCommand != null)
                {
                    var tile = Info.model.Scene.GetTile(Info.ActiveEntity.tile.Position + vectorCommand.Vector);
                    if (tile != null)
                        Info.TargetEntity = tile.Entity;
                }

                if (!Info.hasTargetEntity)
                {
                    Info.model.Messages.Add("Okay, then.");
                    State = ReturnCode.Cancel;
                    return;
                }
            }

            // hit chance
            int dc = 15;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                dc -= 5;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                dc -= 5;
            if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Expert)
                dc += 5;
            
            bool success = new Die(20).Result > dc;

            if (success)
            {
                var awakeBeforeSweep = Info.TargetEntity.health.Awake;

                var force = new Die(2).Result - 1;
                Info.TargetEntity.health.ApplyKnockoutDamage(force);

                BaseEventInfo newInfo = new BaseEventInfo(Info.model);
                newInfo.ActiveEntity = Info.TargetEntity;
                newInfo.Verbose = false;
                Info.model.Events.Add(new FallEvent(newInfo));

                if (Info.Verbose)
                {
                    string surface;

                    if (Info.TargetEntity.tile.hasFeature)
                        surface = Info.TargetEntity.tile.Feature.Name;
                    else surface = "ground";

                    Info.model.Messages.Add(Info.ActiveEntity.BaseType + " " + GetAttackVerb(VerbTense.Past) + " " + Info.TargetEntity.BaseType + " to the " + surface + ".");

                    if (!Info.TargetEntity.health.Awake && awakeBeforeSweep)
                        Info.model.Messages.Add(Info.TargetEntity.BaseType + " got knocked out.");
                }

                State = ReturnCode.Done;
                return;
            }

            // reversal chance
            dc = 20;
            if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                dc -= 5;
            if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Expert)
                dc -= 5;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                dc += 2;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                dc += 2;

            success = new Die(20).Result > dc;
            if (success)
            {
                BaseEventInfo stumbleInfo = new BaseEventInfo(Info.model);
                stumbleInfo.ActiveEntity = Info.ActiveEntity;
                Info.model.Events.Add(new StumbleEvent(stumbleInfo));

                if (Info.Verbose)
                    Info.model.Messages.Add(GetReversalDescription());

                State = ReturnCode.Done;
                return;
            }

            // complete whiff
            if (Info.Verbose)
                Info.model.Messages.Add(GetWhiffDescription());

            State = ReturnCode.Done;
            return;
        }

        private string GetWhiffDescription()
        {
            List<string> options = new List<string>();

            options.Add(Info.ActiveEntity.BaseType + "'s " + GetAttackVerb(VerbTense.Abstract) + " missed.");
            options.Add(Info.ActiveEntity.BaseType + " tried to " + GetAttackVerb(VerbTense.Abstract) + ", but " + Info.TargetEntity.BaseType + " blocked it.");

            return (options[new Die(options.Count).Result - 1]);
        }

        private string GetAttackVerb(VerbTense tense)
        {
            List<Verb> options = new List<Verb>();

            options.Add(new Verb("sweep", "swept", "sweeping", "sweep"));

            if (Info.ActiveEntity.stance.current == StanceType.CROUCH || Info.ActiveEntity.stance.current == StanceType.PRONE)
            {
                options.Add(new Verb("snap kick", "snap kicked", "snap kicking", "snap kick"));
                options.Add(new Verb("swipe", "swiped", "swiping", "swipe"));
            }

            switch (Info.ActiveEntity.stance.current)
            {
                case StanceType.STAND:
                    options.Add(new Verb("kick", "kicked", "kicking", "kick"));
                    options.Add(new Verb("knee", "kneed", "kneeing", "knee"));
                    break;

                case StanceType.PRONE:
                    options.Add(new Verb("scissor kick", "scissor kicked", "scissor kicking", "scissor kick"));
                    options.Add(new Verb("ankle lock", "ankle locked", "ankle locking", "ankle lock"));
                    break;

                case StanceType.CROUCH:
                    options.Add(new Verb("leg hook", "leg hooked", "leg hooking", "leg hook"));
                    break;
            }

            switch (tense)
            {
                case VerbTense.Abstract:
                    return (options[new Die(options.Count).Result - 1].Abstract);

                case VerbTense.Future:
                    return (options[new Die(options.Count).Result - 1].Future);

                case VerbTense.Present:
                    return (options[new Die(options.Count).Result - 1].Present);

                default:
                    return (options[new Die(options.Count).Result - 1].Past);
            }
        }

        private string GetReversalDescription()
        {
            List<string> options = new List<string>();
            string attackVerb = GetAttackVerb(VerbTense.Past);

            options.Add(Info.TargetEntity.BaseType + " kicked out " + Info.ActiveEntity.BaseType + "'s " + attackVerb + ".");
            options.Add(Info.TargetEntity.BaseType + " kicked away " + Info.ActiveEntity.BaseType + "'s " + attackVerb + ".");
            options.Add(Info.TargetEntity.BaseType + " stomped on " + Info.ActiveEntity.BaseType + "'s " + attackVerb + ".");
            options.Add(Info.ActiveEntity.BaseType + " " + attackVerb + " got kicked away.");
            options.Add(Info.ActiveEntity.BaseType + " tried to " + attackVerb + ", but " + Info.TargetEntity.BaseType + " blocked it.");

            return (options[new Die(options.Count).Result - 1]);
        }
    }

    
}
