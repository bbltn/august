﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public enum VerbTense { Past, Present, Future, Abstract };

    class PunchEvent : BaseEvent
    {
        public PunchEvent(IEventInfo info)
            : base(info)
        {
            if (Info.ActiveEntity.skills.CheckAptitude("Striking") == AptitudeLevel.Expert)
                Occurrence = EventOccurrence.Immediate;
            else
                Occurrence = EventOccurrence.Delayed;
        }


        public override void Process()
        {
            if (Info.ActiveEntity == null || Info.TargetEntity == null)
            {
                State = ReturnCode.Cancel;
                return;
            }

            if (!VecUtils.Adjacenct(Info.ActiveEntity.tile.Position, Info.TargetEntity.tile.Position))
            {
                State = ReturnCode.Cancel;
                return;
            }

            

            // enemy dodge chance
            int dc = 15;
            if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                dc -= 5;
            if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Expert)
                dc -= 5;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Expert)
                dc += 5;

            bool success = new Die(20).Result >= dc;
            if (!Info.TargetEntity.health.Awake)
                success = false;
            if (!Info.TargetEntity.health.Alive)
                success = false;

            if (success) // dodge
            {

                if (Info.Verbose)
                    Info.model.Messages.Add(GetDodgeDescription());


                // reversal sweep chance
                dc = 20;
                if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                    dc -= 3;
                if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                    dc -= 3;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Trained)
                    dc += 2;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Expert)
                    dc += 2;
                success = new Die(20).Result >= dc;
                if (success)
                {
                    var sweepInfo = new BaseEventInfo(Info.model);
                    sweepInfo.ActiveEntity = Info.TargetEntity;
                    sweepInfo.TargetEntity = Info.ActiveEntity;
                    sweepInfo.Cost = 0;
                    Info.model.Events.Add(new SweepEvent(sweepInfo));

                    State = ReturnCode.Done;
                    return;
                }

                // attacker stumble chance
                dc = 10;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Trained)
                    dc -= 5;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Expert)
                    dc -= 5;
                success = new Die(20).Result >= dc;
                if (!success)
                {
                    var stumbleInfo = new BaseEventInfo(Info.model);
                    stumbleInfo.ActiveEntity = Info.ActiveEntity;
                    Info.model.Events.Add(new StumbleEvent(stumbleInfo));

                    State = ReturnCode.Done;
                    return;
                }

                State = ReturnCode.Done;
                return;
            }

            //hit chance
            dc = 10;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Trained)
                dc -= 5;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Expert)
                dc -= 5;

            success = new Die(20).Result >= dc;
            if (!Info.TargetEntity.health.Awake)
                success = true;
            if (!Info.TargetEntity.health.Alive)
                success = true;

            if (success)
            {
                bool awakeBeforeAttack = Info.TargetEntity.health.Awake;

                int damage = new Die(GetMaxDamage()).Result;

                Info.TargetEntity.health.ApplyKnockoutDamage(damage);
                if (Info.Verbose)
                    Info.model.Messages.Add(Info.ActiveEntity.BaseType + " " + GetActionVerb(VerbTense.Past) + " " + Info.TargetEntity.BaseType + " in the " + GetBodyPart() + ".");

                if (awakeBeforeAttack && !Info.TargetEntity.health.Awake)
                {
                    var proneInfo = new BaseEventInfo(Info.model);
                    proneInfo.ActiveEntity = Info.TargetEntity;
                    Info.model.Events.Add(new FallEvent(proneInfo));

                    if (Info.Verbose)
                        Info.model.Messages.Add(Info.TargetEntity.BaseType + " passed out.");
                }
            }

            //if no hit
            else
            {
                if (Info.Verbose)
                    Info.model.Messages.Add(GetMissDescription());

                // stumble chance
                dc = 10;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Trained)
                    dc -= 5;
                if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Expert)
                    dc -= 5;
                if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Expert)
                    dc += 5;

                success = new Die(20).Result >= dc;

                if (success)
                {
                    BaseEventInfo newInfo = new BaseEventInfo(Info.model);
                    newInfo.ActiveEntity = Info.TargetEntity;
                    Info.model.Events.Add(new StumbleEvent(newInfo));
                }
            }

            State = ReturnCode.Done;
            return;
        }

        private int GetMaxDamage()
        {
            int maxDamage = 1;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Trained)
                maxDamage++;
            if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Expert)
                maxDamage++;
            if ((int)Info.TargetEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Expert)
                maxDamage = Math.Max(maxDamage - 1, 1);
            return maxDamage;
        }

        private string GetMissDescription()
        {
            List<string> forms = new List<string>();

            forms.Add(Info.ActiveEntity.BaseType + " threw a " + GetActionVerb(VerbTense.Abstract) + " and missed.");
            forms.Add(Info.ActiveEntity.BaseType + " " + GetActionVerb(VerbTense.Past) + " at " + Info.TargetEntity.BaseType + " but missed.");
            forms.Add(Info.ActiveEntity.BaseType + "'s " + GetActionVerb(VerbTense.Abstract) + " missed " + Info.TargetEntity.BaseType + ".");

            return forms[new Die(forms.Count - 1).Result];
            //return forms[0];
        }

        private string GetDodgeDescription()
        {
            List<string> forms = new List<string>();

            forms.Add(Info.TargetEntity.BaseType + " dodged " + Info.ActiveEntity.BaseType + "'s attack.");
            forms.Add(Info.TargetEntity.BaseType + " ducked the attack from " + Info.ActiveEntity.BaseType + ".");
            forms.Add(Info.TargetEntity.BaseType + " slipped past " + Info.ActiveEntity.BaseType + "'s attack.");
            forms.Add(Info.TargetEntity.BaseType + " evaded " + Info.ActiveEntity.BaseType + ".");
            forms.Add(Info.ActiveEntity.BaseType + "'s " + GetActionVerb(VerbTense.Abstract) + " sailed past " + Info.TargetEntity.BaseType + ".");
            forms.Add(Info.TargetEntity.BaseType + " dodged " + Info.ActiveEntity.BaseType + "'s " + GetActionVerb(VerbTense.Abstract) + ".");

            return forms[new Die(forms.Count - 1).Result];
        }

        private static string GetActionVerb(VerbTense tense)
        {
            string actionVerb = "";
            switch (new Die(6).Result)
            {
                case 1:
                    if (tense == VerbTense.Past) actionVerb = "hit";
                    if (tense == VerbTense.Present) actionVerb = "hits";
                    if (tense == VerbTense.Future) actionVerb = "will hit";
                    if (tense == VerbTense.Abstract) actionVerb = "hit";
                    break;

                case 2:
                    if (tense == VerbTense.Past) actionVerb = "jabbed";
                    if (tense == VerbTense.Present) actionVerb = "jabs";
                    if (tense == VerbTense.Future) actionVerb = "will jab";
                    if (tense == VerbTense.Abstract) actionVerb = "jab";
                    break;

                case 3:
                    if (tense == VerbTense.Past) actionVerb = "elbowed";
                    if (tense == VerbTense.Present) actionVerb = "elbows";
                    if (tense == VerbTense.Future) actionVerb = "will elbow";
                    if (tense == VerbTense.Abstract) actionVerb = "elbow strike";
                    break;

                case 4:
                    if (tense == VerbTense.Past) actionVerb = "ridgehanded";
                    if (tense == VerbTense.Present) actionVerb = "ridge hand strikes";
                    if (tense == VerbTense.Future) actionVerb = "will ridgehand";
                    if (tense == VerbTense.Abstract) actionVerb = "ridge hand";
                    break;

                case 5:
                    if (tense == VerbTense.Past) actionVerb = "struck";
                    if (tense == VerbTense.Present) actionVerb = "strikes";
                    if (tense == VerbTense.Future) actionVerb = "will strike";
                    if (tense == VerbTense.Abstract) actionVerb = "strike";
                    break;

                default:
                    if (tense == VerbTense.Past) actionVerb = "punched";
                    if (tense == VerbTense.Present) actionVerb = "punches";
                    if (tense == VerbTense.Future) actionVerb = "will punch";
                    if (tense == VerbTense.Abstract) actionVerb = "punch";
                    break;
            }

            return actionVerb;
        }


        private static string GetBodyPart()
        {
            string actionVerb;
            switch (new Die(11).Result)
            {
                case 1:
                    actionVerb = "head";
                    break;

                case 2:
                    actionVerb = "face";
                    break;

                case 3:
                    actionVerb = "sternum";
                    break;

                case 4:
                    actionVerb = "gut";
                    break;

                case 5:
                    actionVerb = "kidney";
                    break;

                case 6:
                    actionVerb = "side of the neck";
                    break;

                case 7:
                    actionVerb = "chin";
                    break;

                case 8:
                    actionVerb = "bridge of the nose";
                    break;

                case 9:
                    actionVerb = "temple";
                    break;

                case 10:
                    actionVerb = "groin";
                    break;

                default:
                    actionVerb = "neck";
                    break;
            }
            return actionVerb;
        }
    }
}
