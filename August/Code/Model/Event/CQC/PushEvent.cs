﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class PushEvent : BaseEvent
    {
        public bool askedForEntity = false;

        public PushEvent(IEventInfo info)
            : base(info)
        {
            Occurrence = EventOccurrence.Immediate;
        }

        public override void Process()
        {
            // find out who to push
            if (Info.TargetEntity == null)
            {
                AcquireTargetEntity();
                return;
            }

            // check for reversal
            if (Info.TargetEntity.health.Awake && Info.TargetEntity.health.Alive)
            {
                int dc = 15;
                if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                    dc -= 5;
                if ((int)Info.TargetEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Expert)
                    dc -= 5;


                if ((int)Info.ActiveEntity.skills.CheckAptitude("Judo") >= (int)AptitudeLevel.Trained)
                    dc += 5;
                // why else: being a trained attacker will give you a push benefit, but limited. no stacking from different disciplines.
                else if ((int)Info.ActiveEntity.skills.CheckAptitude("Striking") >= (int)AptitudeLevel.Trained)
                    dc += 5;

                var roll = new Die(20);

                if (roll.Succeeds(dc) || roll.NaturalCrit)
                {
                    BaseEventInfo stumbleInfo = new BaseEventInfo(Info.model);
                    stumbleInfo.ActiveEntity = Info.ActiveEntity;
                    Info.model.Events.Add(new StumbleEvent(stumbleInfo));

                    if (Info.Verbose)
                        Info.model.Messages.Add(GetReversalDescription());

                    State = ReturnCode.Done;
                    return;
                }
            }


            // check for hit
            BaseEventInfo launchInfo = new BaseEventInfo(Info.model);
            launchInfo.ActiveEntity = Info.TargetEntity;
            launchInfo.Vector = Info.TargetEntity.tile.Position - Info.ActiveEntity.tile.Position;
            launchInfo.Counter = 2;
            Info.model.Events.Add(new LaunchEvent(launchInfo));

            if (Info.Verbose)
            {
                 Info.model.Messages.Add(Info.ActiveEntity.BaseType + " " + GetDescriptor() + " " + Info.TargetEntity.BaseType + ".");
            }

            State = ReturnCode.Done;
            return;
        }

        private string GetReversalDescription()
        {
            List<string> options = new List<string>();

            options.Add(Info.TargetEntity.BaseType + " reverses " + Info.ActiveEntity.BaseType + "'s push.");

            return (options[new Die(options.Count).Result - 1]);
        }

        private string GetDescriptor()
        {
            switch (new Die(3).Result)
            {
                case 1:
                    return " kicked ";

                case 2:
                    return " shoved ";

                default:
                    return "pushed";
            }
        }
    }
}
