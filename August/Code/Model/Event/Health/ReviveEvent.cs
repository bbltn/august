﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    class ReviveEvent : BaseEvent
    {
        public bool askedForEntity = false;

        public ReviveEvent(IEventInfo info)
            : base(info)
        {
            SetCostByAptitude("Combat Medicine", 6, 12, 20);
            Occurrence = EventOccurrence.Delayed;
        }

        
        public override void Process()
        {
            // find out who to revive
            if (Info.TargetEntity == null)
            {
                AcquireTargetEntity();
                return;
            }

            if (Info.Verbose)
                Info.model.Messages.Add(Info.ActiveEntity.BaseType + " trying to revive " + Info.TargetEntity.BaseType);

            // can't revive the dead
            if (!Info.TargetEntity.health.Alive)
            {
                State = ReturnCode.Cancel;
                return;
            }

            // can't revive the conscious
            if (Info.TargetEntity.health.Awake)
            {
                State = ReturnCode.Cancel;
                return;
            }

            int max = 1;
            if (Info.ActiveEntity.skills.CheckAptitude("Combat Medicine") >= AptitudeLevel.Trained)
                max++;
            if (Info.ActiveEntity.skills.CheckAptitude("Combat Medicine") >= AptitudeLevel.Expert)
                max++;

            var roll = new Die(max);
            Info.TargetEntity.health.HealKnockoutDamage(roll.Result);

            if (Info.Verbose)
                Info.model.Messages.Add(Info.ActiveEntity.BaseType + " revived " + Info.TargetEntity.BaseType + ".");

            State = ReturnCode.Done;
            return;
        }
    }
}