﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public enum AlarmLevel { Calm, Suspect, Alert, Combat }
    /* calm - just a regular patrol
     * suspect - signs of passive intrusion, strange noises or objects
     * alert - signs of enemy intrusion, spotting one, finding blood or dead friendlies, explosions etc.
     * combat - actively engaged with an enemy force */

    public class Knowledge
    {
        private IEntity Self;
        public List<IEntity> Friends;
        public List<IEntity> Enemies;
        public AlarmLevel Alarm        { get; private set; }
        public AlarmLevel HighestAlarm { get; private set; }

        public Knowledge(IEntity self)
        {
            Friends = new List<IEntity>();
            Enemies = new List<IEntity>();
            Alarm = AlarmLevel.Calm;
            HighestAlarm = AlarmLevel.Calm;
            Self = self;
        }

        public List<IEntity> GetAdjacentEnemies()
        {
            List<IEntity> adjacents = new List<IEntity>();

            foreach (var ent in Enemies)
            {
                if (Self.tile != null & ent.tile != null)
                    if (VecUtils.Adjacenct(Self.tile.Position, ent.tile.Position))
                        adjacents.Add(ent);
            }

            return adjacents;
        }

        public List<IEntity> GetAdjacentFriends()
        {
            List<IEntity> adjacents = new List<IEntity>();

            foreach (var ent in Friends)
            {
                if (Self.tile != null & ent.tile != null)
                    if (VecUtils.Adjacenct(Self.tile.Position, ent.tile.Position))
                        adjacents.Add(ent);
            }

            return adjacents;
        }

        public List<IEntity> GetAdjacent()
        {
            List<IEntity> adjacents = new List<IEntity>();

            var budds = GetAdjacentFriends();
            var foes = GetAdjacentEnemies();

            foreach (var bud in budds)
                adjacents.Add(bud);

            foreach (var foe in foes)
                adjacents.Add(foe);
            
            return adjacents;
        }

        public void DecrementAlarm()
        {
            switch (Alarm)
            {
                case AlarmLevel.Combat:
                    SetAlarm(AlarmLevel.Alert);
                    break;
                case AlarmLevel.Alert:
                    SetAlarm(AlarmLevel.Suspect);
                    break;
                case AlarmLevel.Suspect:
                    SetAlarm(AlarmLevel.Calm);
                    break;
                default:
                    SetAlarm(AlarmLevel.Calm);
                    break;
            }
        }

        public void Update(Model model, IEntity actor)
        {
            ObserveEntities(model, actor);

            if (Enemies.Count > 0)
                SetAlarm(AlarmLevel.Combat);

            if (Enemies.Count == 0 && Alarm == AlarmLevel.Combat)
                SetAlarm(AlarmLevel.Alert);
        }


        public void SetAlarm(AlarmLevel value)
        {
            Alarm = value;

            if ((int)value > (int)HighestAlarm)
                HighestAlarm = value;
        }


        private void ObserveEntities(Model model, IEntity actor)
        {
            foreach (var ent in model.Scene.getEntities)
            {
                if (ent != actor)
                {
                    if (model.Scene.LineOfSight(actor.tile.Position, ent.tile.Position))
                    {
                        if (ent.BaseType != actor.BaseType)
                        {
                            if (!Enemies.Contains(ent))
                                Enemies.Add(ent);
                        }
                        else
                        {
                            if (!Friends.Contains(ent))
                                Friends.Add(ent);
                        }
                    }

                    else
                    {
                        if (ent.BaseType != actor.BaseType)
                        {
                            if (Enemies.Contains(ent))
                                Enemies.Remove(ent);
                        }
                        else
                        {
                            if (!Friends.Contains(ent))
                                Friends.Remove(ent);
                        }
                    }
                }
            }
        }
    }
}
