﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public delegate void ChangedEventHandler(object sender, EventArgs e);


    public interface IHealth
    {
        event ChangedEventHandler Change;
        event ChangedEventHandler Death;
        event ChangedEventHandler Resurrection;
        event ChangedEventHandler Knockout;
        event ChangedEventHandler Resuscitation;

        bool Alive { get; }
        bool Awake { get; }

        int curHP { get; }
        int curKO { get; }

        int maxHP { get; set; }
        int maxKO { get; set; }

        void SetHP(int value);
        void SetKO(int value);

        void ApplyFatalDamage(int value);
        void ApplyKnockoutDamage(int value);

        void HealFatalDamage(int value);
        void HealKnockoutDamage(int value);

        void ResetFatalDamage();
        void ResetKnockoutDamage();
    }


    public class Health : IHealth
    {
        public event ChangedEventHandler Change;
        protected virtual void OnChanged()
        {
            if (Change != null)
                Change(this, EventArgs.Empty);
        }

        public event ChangedEventHandler Death;
        protected virtual void OnDeath()
        {
            if (Death != null)
                Death(this, EventArgs.Empty);
        }

        public event ChangedEventHandler Resurrection;
        protected virtual void OnResurrection()
        {
            if (Resurrection != null)
                Resurrection(this, EventArgs.Empty);
        }

        public event ChangedEventHandler Knockout;
        protected virtual void OnKnockout()
        {
            if (Knockout != null)
                Knockout(this, EventArgs.Empty);
        }

        public event ChangedEventHandler Resuscitation;
        protected virtual void OnResuscitate()
        {
            if (Resuscitation != null)
                Resuscitation(this, EventArgs.Empty);
        }

        public int curHP { get; private set; }
        public int curKO { get; private set; }

        public int maxHP { get; set; }
        public int maxKO { get; set; }

        public void SetHP(int value)
        {
            if (value == curHP)
                return;
            
            if (value > maxHP)
                curHP = maxHP;
            if (value < 0)
                curHP = 0;
            curHP = value;

            OnChanged();
        }

        public void SetKO(int value)
        {
            if (value == curKO)
                return;

            if (value > maxKO)
                curKO = maxKO;
            if (value < 0)
                curKO = 0;
            curKO = value;

            OnChanged();
        }


        public bool Alive { get { return (curHP > 0); } }
        public bool Awake { get { return (curKO > 0); } }

        public Health(int HP = 5, int KO = 5)
        {
            maxHP = HP;
            ResetFatalDamage();

            maxKO = KO;
            ResetKnockoutDamage();
        }




        public void ApplyFatalDamage(int value)
        {
            if (value <= 0 || curHP <= 0)
                return;

            curHP = Math.Max(curHP - value, 0);
            OnChanged();
            if (!Alive)
                OnDeath();
        }


        public void ApplyKnockoutDamage(int value)
        {
            if (value <= 0 || curKO <= 0)
                return;

            curKO = Math.Max(curKO - value, 0);
            OnChanged();
            if (!Awake)
                OnKnockout();
        }


        public void HealFatalDamage(int value)
        {
            if (curHP >= maxHP)
                return;

            bool startDead = !Alive;
            curHP = Math.Min(curHP + value, maxHP);
            OnChanged();
            if (startDead && Alive)
                OnResurrection();
        }


        public void HealKnockoutDamage(int value)
        {
            if (value <= 0 || curKO >= maxKO)
                return;
            bool startUnconscious = !Awake;
            curKO = Math.Min(curKO + value, maxKO);
            OnChanged();
            if (startUnconscious && Awake)
                OnResuscitate();
        }


        public void ResetFatalDamage()
        {
            if (curHP == maxHP)
                return;
            curHP = maxHP;
            OnChanged();
        }


        public void ResetKnockoutDamage()
        {
            if (curKO == maxKO)
                return;
            curKO = maxKO;
            OnChanged();
        }
    }
}
