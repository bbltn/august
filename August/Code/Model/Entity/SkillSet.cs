﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public interface ISkillSet
    {
        int Count { get; }
        AptitudeLevel CheckAptitude(string name);
        AptitudeLevel CheckAptitude(int index);
        string CheckName(int index);
        void SetAptitude(string name, AptitudeLevel aptitude);
        void Randomize();
    }

    public class SkillSet : ISkillSet
    {
        private List<ISkill> Skills;
        public int Count { get { return Skills.Count; } }

        public SkillSet()
        {
            Skills = new List<ISkill>();
            AddDefaultSkills();
        }

        private void AddDefaultSkills()
        {
            Skills.Add(new Skill("Judo"));
            Skills.Add(new Skill("Striking"));
            Skills.Add(new Skill("Combat Medicine"));
            Skills.Add(new Skill("Point Shooting"));
            Skills.Add(new Skill("Aimed Shooting"));
        }

        public AptitudeLevel CheckAptitude(string name)
        {
            var skill = FindSkill(name);
            if (skill != null)
                return skill.Aptitude;
            return AptitudeLevel.Untrained;
        }

        public AptitudeLevel CheckAptitude(int index)
        {
            var skill = FindSkill(index);
            if (skill != null)
                return skill.Aptitude;
            return AptitudeLevel.Untrained;
        }

        public string CheckName(int index)
        {
            var skill = FindSkill(index);
            if (skill != null)
                return skill.Name;
            return "no_name_skill";
        }


        public void SetAptitude(string name, AptitudeLevel aptitude)
        {
            foreach (var skill in Skills)
                if (skill.Name == name)
                {
                    skill.Aptitude = aptitude;
                    return;
                }
        }

        public void Randomize()
        {
            foreach (var skill in Skills)
                skill.Aptitude = (AptitudeLevel)(new Die(3).Result - 1);
        }

        private ISkill FindSkill(string name)
        {
            foreach (ISkill skill in Skills)
                if (skill.Name == name)
                    return skill;
            return null;
        }

        private ISkill FindSkill(int index)
        {
            if (index >= 0 && index <= Skills.Count)
                return Skills[index];
            return null;
        }
    }
}
