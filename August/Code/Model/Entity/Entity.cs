﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;
using Bramble.Core;

namespace August
{
    public interface IEntity
    {
        Character Graphic { get; set; }
        Tile tile { get; set; }
        bool IsPlayer { get; set; }
        string BaseType { get; set; }
        Knowledge knowledge { get; set; }
        IHealth health { get; }
        IStance stance { get; }
        ISkillSet skills { get; }
        void UpdateSystems(Model model);
        IGameEvent curEvent { get; set; }
    }

    public class BaseEntity : IEntity
    {
        public Character Graphic { get; set; }
        public Tile tile { get; set; }        
        public bool IsPlayer { get; set; }
        public string BaseType { get; set; }
        private const string BASE_TYPE = "BASE_TYPE";
        public Knowledge knowledge { get; set; }
        public IHealth health { get; private set; }
        public IStance stance { get; private set; }
        public ISkillSet skills { get; private set; }
        public IGameEvent curEvent { get; set; }

        public BaseEntity(IScene scene)
        {
            Graphic = new Character('&', TermColor.White);
            knowledge = new Knowledge(this);
            BaseType = BASE_TYPE;
            health = new Health(5, 5);
            stance = new Stance();
            skills = new SkillSet();
        }

        public void UpdateSystems(Model model)
        {
            knowledge.Update(model, this);
            UpdateGraphic();
        }

        private void UpdateGraphic()
        {
            var deadColor = TermColor.Red;
            var koColor = TermColor.Blue;

            if (health.Alive == false && Graphic.BackColor != deadColor)
                Graphic = new Character(Graphic.Glyph, Graphic.ForeColor, deadColor);

            else if (health.Awake == false && Graphic.BackColor != koColor)
                Graphic = new Character(Graphic.Glyph, Graphic.ForeColor, koColor);

            else if ((health.Alive && Graphic.BackColor == deadColor) || (health.Awake && Graphic.BackColor == koColor))
                Graphic = new Character(Graphic.Glyph, Graphic.ForeColor);
        }
    }
}
