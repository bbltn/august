﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public enum StanceType { STAND, CROUCH, PRONE }

    public interface IStance
    {
        event ChangedEventHandler Change;
        event ChangedEventHandler Stand;
        event ChangedEventHandler Crouch;
        event ChangedEventHandler Prone;

        StanceType current { get; }
        void SetStance(StanceType stance);
    }

    public class Stance : IStance
    {
        public event ChangedEventHandler Change;
        protected virtual void OnChange()
        {
            if (Change != null)
                Change(this, EventArgs.Empty);
        }

        public event ChangedEventHandler Stand;
        protected virtual void OnStand()
        {
            if (Stand != null)
                Stand(this, EventArgs.Empty);
        }

        public event ChangedEventHandler Crouch;
        protected virtual void OnCrouch()
        {
            if (Crouch != null)
                Crouch(this, EventArgs.Empty);
        }

        public event ChangedEventHandler Prone;
        protected virtual void OnProne()
        {
            if (Prone != null)
                Prone(this, EventArgs.Empty);
        }

        public StanceType current { get; private set; }



        public Stance(StanceType startingStance = StanceType.STAND)
        {
            SetStance(startingStance);
        }

        public void SetStance(StanceType stance)
        {
            if (current == stance)
                return;

            current = stance;

            if (current == StanceType.STAND) OnStand();
            if (current == StanceType.CROUCH) OnCrouch();
            if (current == StanceType.PRONE) OnProne();
        }
    }
}
