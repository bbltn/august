﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public enum AptitudeLevel { Untrained, Trained, Expert }

    public interface ISkill
    {
        string Name { get; }
        AptitudeLevel Aptitude { get; set; }
        void Increment();
    }

    public class Skill : ISkill
    {
        public string Name { get; protected set; }
        public AptitudeLevel Aptitude { get; set; }

        public Skill(string name, AptitudeLevel aptitude = AptitudeLevel.Untrained)
        {
            Name = name;
            Aptitude = aptitude;
        }

        public void Increment()
        {
            if ((int)Aptitude < (int)AptitudeLevel.Expert)
                Aptitude++;
        }

        public void Decrement()
        {
            if ((int)Aptitude > (int)AptitudeLevel.Untrained)
                Aptitude--;
        }
    }
}
