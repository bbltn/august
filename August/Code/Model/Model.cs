﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    public class Model
    {
        public float FPS = 0;
        public bool autosave = false;

        public IEventCollection Events { get; private set; }
        public ITurnSchedule Schedule { get; private set; }
        public IMessageLog Messages { get; private set; }
        public IScene Scene;
        public List<BaseCommand> Commands = new List<BaseCommand>();
        
        public bool NeedsCommand
            { get { /*if (Events.IsEmpty)*/ return true; /*else return false;*/ } }

        public Model()
        {
            Messages = new MessageLog();

            SceneData initialSceneData = new SceneData();
            Scene = new SceneImplementation(initialSceneData, Messages);
            Scene.LoadScene();
            Scene.LoadEntities();
            Messages.sceneTime = Scene.Time;

            Events = new EventCollection();
            Schedule = new TurnSchedule(Scene.getEntities);
        }


        public void Restart()
        {
            Messages = new MessageLog();

            SceneData newSceneData = new SceneData();
            Scene = new SceneImplementation(newSceneData, Messages);
            Scene.LoadScene();
            Scene.CreateDefaultEntities();

            Events = new EventCollection();
            Schedule = new TurnSchedule(Scene.getEntities);
        }

        public void Reload()
        {
            Messages = new MessageLog();

            SceneData newSceneData = new SceneData();
            Scene = new SceneImplementation(newSceneData, Messages);
            Scene.LoadScene();
            Scene.LoadEntities();

            Events = new EventCollection();
            Schedule = new TurnSchedule(Scene.getEntities);
        }

        public void Update()
        {
            Events.Update();

            if (Events.IsResolved)
            {
                Schedule.Update();
                var list = Schedule.ReadyActors;
                foreach (var guy in list)
                    DoActorTurn(guy);

                if (list.Count > 0)
                {
                    return;
                }
            }

            //Events.Update();

            if (Events.IsResolved)
                Scene.Time.Increment();
            
            Scene.Update(); // why so late?
            
            if (autosave)
                FileOperations.Save(Scene); // when to do this?
        }

        private void DoActorTurn(IEntity actor)
        {
            actor.UpdateSystems(this);

            if (actor.curEvent != null) return;

            if (actor.IsPlayer)
            {
                PlayerInput.CommandsToEvents(this, actor);
            }
            else
                AI.CalculateEvent(this, actor);
        }

        public void AddCommand(BaseCommand command)
        {
            Commands.Add(command);
        }  
    }
}
