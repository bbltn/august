﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{

    public interface ITurnSchedule
    {
        void Update();
        //IEntity NextActor { get; }
        //void ApplyCost(IEntity targetEntity, int cost);
        //int GetEnergyLevel(IEntity targetEntity);
        int Count { get; }
        void AssignNewEntityList(List<IEntity> entities);
        List<IEntity> ReadyActors { get; }
    }
    
    public class TurnSchedule : ITurnSchedule
    {
        public const int ENERGY_THRESHOLD = 100;
        public int Count { get { return actors.Count; } }

        private List<IEntity> actors;
        public List<IEntity> ReadyActors 
        {
            get
            {
                List<IEntity> returnList = new List<IEntity>();

                foreach (var actor in actors)
                    if (actor.curEvent == null)
                        returnList.Add(actor);

                return returnList;
            }
        }
        
        public TurnSchedule(List<IEntity> entities)
        {
            actors = entities;
        }

        public void AssignNewEntityList(List<IEntity> entities)
        {
            actors = entities;
        }

        public void Update()
        {
            //ClearFinishedEvents();
        }

        private void ClearFinishedEvents()
        {
            foreach (var actor in actors)
            {
                if (actor.curEvent != null)
                {
                    if (actor.curEvent.State == ReturnCode.Cancel)
                        actor.curEvent = null;
                    //if (actor.curEvent.State == ReturnCode.Done && actor.curEvent.


                }
            }
        }

        /*
        public IEntity NextActor 
        {
            get
            {
                IEntity toReturn = null;

                for (int iter = 0; iter < actors.Count; iter++)
                {
                    if (actors[iter].Energy >= ENERGY_THRESHOLD)
                    {
                        if (toReturn == null)
                            toReturn = actors[iter];

                        else if (actors[iter].Energy > toReturn.Energy)
                            toReturn = actors[iter];

                        else
                            actors[iter].Energy = toReturn.Energy - 1;
                    }
                }

                return toReturn;
            }
        }
         */
        /*
        public void ApplyCost(IEntity targetEntity, int cost)
        {
            targetEntity.Energy -= cost;
        }

        public int GetEnergyLevel(IEntity targetEntity)
        {
            for (int iter = 0; iter < actors.Count; iter++)
            {
                if (actors[iter] == targetEntity)
                {
                    return actors[iter].Energy;
                }
            }

            return -1;
        }
         */
    }
}
