﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public interface ITime
    {
        int Current { get; }
        void Increment();
        void Randomize();
        void Set(string clockTime);
    }

    public class SceneTime : ITime
    {
        private int time;

        public int Current { get { return time; } }

        public SceneTime()
        {
            Set("00:00");
        }

        public void Increment() 
        { 
            time++; 
        }

        public void Randomize()
        {
            int hours = new Die(24).Result - 1;
            int mins = new Die(60).Result - 1;
            Set(TimeUtils.FormatClockTime(hours, mins));
        }

        public void Set(string clockTime)
        {
            time = TimeUtils.ClockTimeToUnits(clockTime);
        }
    }
}
