﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public enum DayPhase { NIGHT, DAWN, MORNING, NOON, AFTERNOON, DUSK, EVENING }

    public static class TimeUtils
    {
        public static DayPhase GetTimeOfDay(int timeUnits)
        {
            float seconds = UnitsToSeconds(timeUnits);

            int minutes = 0;
            while (seconds >= 60)
            {
                minutes++;
                seconds -= 60;
            }

            int hours = 0;
            while (minutes >= 60)
            {
                hours++;
                minutes -= 60;
            }

            while (hours > 24)
                hours -= 24;

            if (hours >= 0 && hours < 5)
                return DayPhase.NIGHT;
            if (hours >= 5 && hours < 8)
                return DayPhase.DAWN;
            if (hours >= 8 && hours < 11)
                return DayPhase.MORNING;
            if (hours >= 11 && hours < 14)
                return DayPhase.NOON;
            if (hours >= 14 && hours < 17)
                return DayPhase.AFTERNOON;
            if (hours >= 17 && hours < 20)
                return DayPhase.DUSK;
            if (hours >= 20 && hours <= 24)
                return DayPhase.EVENING;

            return DayPhase.NOON;
        }

        public static string UnitsToString(int timeUnits, bool abbreviate = false)
        {
            //return timeUnits.ToString();
            
            float seconds = UnitsToSeconds(timeUnits);

            int minutes = 0;
            while (seconds >= 60)
            {
                minutes++;
                seconds -= 60;
            }

            int hours = 0;
            while (minutes >= 60)
            {
                hours++;
                minutes -= 60;
            }

            if (abbreviate)
                return IntToClockFormat((int)minutes) + ":" + IntToClockFormat((int)seconds);
            else
                return IntToClockFormat((int)hours) + ":" + IntToClockFormat((int)minutes) + ":" + IntToClockFormat((int)seconds) + "s";
        }

        public static string FormatClockTime(int hours, int mins)
        {
            return hours.ToString() + ":" + mins.ToString();
        }

        public static string IntToClockFormat(int value)
        {
            if (value > 99) return "99";
            if (value > 9) return value.ToString();

            switch (value)
            {
                case 1:
                    return "01";
                case 2:
                    return "02";
                case 3:
                    return "03";
                case 4:
                    return "04";
                case 5:
                    return "05";
                case 6:
                    return "06";
                case 7:
                    return "07";
                case 8:
                    return "08";
                case 9:
                    return "09";
            }

            return "00";
        }

        public static int ClockTimeToUnits(string wallTime)
        {
            if (wallTime.Length != 5)
                return 0;

            string hourString = wallTime[0].ToString() + wallTime[1].ToString();
            string minuteString = wallTime[3].ToString() + wallTime[4].ToString();

            return HoursToUnits(int.Parse(hourString)) + MinutesToUnits(int.Parse(minuteString));
            //return int.Parse(minuteString);
        }

        public static int HoursToUnits(int hours)
        {
            return MinutesToUnits(hours * 60);
        }

        public static int MinutesToUnits(int minutes)
        {
            return SecondsToUnits(minutes * 60);
        }

        public static int SecondsToUnits(float seconds)
        {
            return (int)(seconds * 2);
        }

        public static float UnitsToSeconds(int timeUnits)
        {
            return timeUnits/2;
        }
    }
}
