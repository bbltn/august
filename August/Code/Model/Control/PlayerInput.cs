﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public static class PlayerInput
    {

        public static void CommandsToEvents(Model model, IEntity actor)
        {
            if (model.Commands.Count <= 0)
                return;

            BaseCommand command = model.Commands[0];
            model.Commands.Clear();

            var info = new BaseEventInfo(model);
                info.ActiveEntity = actor;

            VectorCommand vectorCommand = command as VectorCommand;
            if (vectorCommand != null)
            {

                info.Vector = vectorCommand.Vector;

                var tile = model.Scene.GetTile(info.ActiveEntity.tile.Position + info.Vector);
                
                if (tile != null && tile.hasEntity)
                {
                    info.TargetEntity = tile.Entity;
                    model.Events.Add(new PunchEvent(info));
                }
                else
                    model.Events.Add(new MoveEvent(info));

                return;
            }


            LetterCommand letterCommand = command as LetterCommand;
            if (letterCommand != null)
            {
                switch (letterCommand.Letter)
                {
                    case '5':
                        model.Events.Add(new WaitEvent(info));
                        break;

                    case 'h':
                        model.Messages.Add(model.Scene.GetAdjacentUnblockedTiles(model.Scene.getPlayer.tile.Position).Count.ToString());
                        break;

                    case 'z':
                        model.Events.Add(new ProneEvent(info));
                        break;

                    case 'x':
                        model.Events.Add(new CrouchEvent(info));
                        break;

                    case 'c':
                        model.Events.Add(new StandEvent(info));
                        break;

                    case 'v':
                        model.Events.Add(new ReviveEvent(info));
                        break;

                    case 'a':
                        model.Events.Add(new TakedownEvent(info));
                        break;

                    case 'd':
                        model.Events.Add(new PushEvent(info));
                        break;

                    case 'f':
                        model.Events.Add(new SweepEvent(info));
                        break;

                    case '-':
                        actor.health.ApplyFatalDamage(1);
                        model.Events.Add(new WaitEvent(info));
                        break;

                    case '=':
                        actor.health.HealFatalDamage(1);
                        model.Events.Add(new WaitEvent(info));
                        break;

                    case '_':
                        actor.health.ApplyKnockoutDamage(1);
                        model.Events.Add(new WaitEvent(info));
                        break;

                    case '+':
                        actor.health.HealKnockoutDamage(1);
                        model.Events.Add(new WaitEvent(info));
                        break;

                    case 'r':
                        model.Restart();
                        break;
                    case 'R':
                        model.Reload();
                        break;

                    case 'q':
                        actor.skills.Randomize();
                        break;

                    case 'Q':
                        model.Messages.Clear();
                        break;

                    case '`':
                        model.Scene.Time.Randomize();
                        break;

                    default:
                        model.Messages.Add(letterCommand.Letter.ToString());
                        break;
                }

                return;
            }

        }
    }
}
