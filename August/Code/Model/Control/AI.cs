﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public static class AI
    {
        static bool NEVER_DO_ANYTHING = false;

        public static void CalculateEvent(Model model, IEntity actor)
        {
            var info = new BaseEventInfo(model);
            info.ActiveEntity = actor;

            //if (actor.Energy < 100)
            //    return;

            if (NEVER_DO_ANYTHING)
            {
                model.Events.Add(new WaitEvent(info));
                return;
            }

            

            if (actor.health.Alive == false || actor.health.Awake == false)
            {
                model.Events.Add(new WaitEvent(info));
                return;
            }

            // if friendlies need help
            foreach (var buddy in actor.knowledge.Friends)
            {
                if (buddy.tile != null)
                {                
                    var enemy = GetNearestEnemy(actor);
                    float enemyDistance = 200;
                    if (enemy != null)
                        enemyDistance = VecUtils.Distance(actor.tile.Position, enemy.tile.Position);

                    float buddyDistance = VecUtils.Distance(actor.tile.Position, buddy.tile.Position);

                    if (!buddy.health.Awake && buddy.health.Alive && buddyDistance < enemyDistance)
                    {
                        var path = Navigation.CreatePath(model, actor.tile.Position, buddy.tile.Position);

                        // can't find a path
                        if (path == null)
                        {
                            if (actor.stance.current != StanceType.CROUCH)
                            {
                                model.Events.Add(new CrouchEvent(info));
                                return;
                            }

                            model.Events.Add(new WaitEvent(info));
                            return;
                        }

                        if (actor.stance.current != StanceType.STAND)
                        {
                            model.Events.Add(new StandEvent(info));
                            return;
                        }

                        info.Vector = new Vec(path[0].X - actor.tile.Position.X, path[0].Y - actor.tile.Position.Y);

                        // IS IT TIME TO HEAL?
                        var thing = model.Scene.GetTile(actor.tile.Position + info.Vector);
                        if (thing.hasEntity)
                        {
                            var reviveInfo = new BaseEventInfo(info.model);
                            reviveInfo.ActiveEntity = actor;
                            reviveInfo.TargetEntity = thing.Entity;
                            model.Events.Add(new ReviveEvent(reviveInfo));
                            return;
                        }

                        model.Events.Add(new MoveEvent(info));
                        return;
                    }
                }
            }

            // enemies in sight
            if (actor.knowledge.Enemies.Count > 0)
            {
                IEntity target = GetNearestEnemy(actor);
                var path = Navigation.CreatePath(model, actor.tile.Position, target.tile.Position);

                if (path == null)
                {
                    if (actor.stance.current != StanceType.CROUCH)
                    {
                        model.Events.Add(new CrouchEvent(info));
                        return;
                    }

                    model.Events.Add(new WaitEvent(info));
                    return;
                }

                if (actor.stance.current != StanceType.STAND)
                {
                    model.Events.Add(new StandEvent(info));
                    return;
                }

                info.Vector = new Vec(path[0].X - actor.tile.Position.X, path[0].Y - actor.tile.Position.Y);

                // IS IT TIME TO RUMBLE?
                var thing = model.Scene.GetTile(actor.tile.Position + info.Vector);
                if (thing.hasEntity)
                {
                    info.TargetEntity = thing.Entity;

                    switch (new Die(3).Result)
                    {
                        default:
                            model.Events.Add(new PunchEvent(info));
                            return;
                    }
                }

                model.Events.Add(new MoveEvent(info));
                return;
            }

            // no enemies in sight
            else
            {
                if (actor.stance.current != StanceType.STAND)
                    model.Events.Add(new StandEvent(info));
                else
                    model.Events.Add(new WaitEvent(info));

                return;
            }
        }

        private static IEntity GetNearestEnemy(IEntity actor)
        {
            IEntity target = null;

            foreach (var enemyEntity in actor.knowledge.Enemies)
            {
                if (target == null) target = enemyEntity;

                else if (VecUtils.Distance(actor.tile.Position, enemyEntity.tile.Position) < VecUtils.Distance(actor.tile.Position, target.tile.Position))
                    target = enemyEntity;
            }
            return target;
        }
    }
}
