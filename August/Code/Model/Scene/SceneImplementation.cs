﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    public interface IScene
    {
        SceneData Data { get; }
        void Update();
        void LoadScene();
        
        Tile GetTile(Vec position);
        List<Tile> GetAdjacentUnblockedTiles(Vec position);
        Tile[,] getTileArray { get; }
        void SetTiles(Tile[,] value);

        bool ContainsPosition(Vec position);
        bool LineOfSight(Vec start, Vec end);

        Vec CameraPosition { get; }
        IMessageLog Messages { get; }
        ITime Time { get; }
                
        List<IEntity> getEntities { get; }
        void AddEntity(IEntity entity, Vec position);
        void RemoveEntity(Vec position);
        int getEntitiesCount { get; }
        IEntity GetEntity(int index);
        IEntity getPlayer { get; }
        void NewEntityList();
        void LoadEntities();
        void CreateDefaultEntities();
    }

    public class SceneImplementation : IScene
    {
        public List<IEntity> getEntities { get { return entities; } }
        public IMessageLog Messages { get; private set; }
        public ITime Time { get; private set; }
        public SceneData Data { get; private set; }
        public bool ContainsPosition(Vec position)
        {
            if (position.X < 0 || position.Y < 0)
                return false;
            if (position.X >= Data.Size.X || position.Y >= Data.Size.Y)
                return false;

            return true;
        }

        public bool LineOfSight(Vec start, Vec end)
        {
            var line = Shape.CreateLine(start, end);
                        
            for (int iter = 1; iter < line.Count - 1; iter++)
            {                
                if (tiles[line[iter].X, line[iter].Y].blocksSight)
                    return false;
            }
            
            return true;
        }

        public List<Tile> GetAdjacentUnblockedTiles(Vec position)
        {
            List<Tile> adjacents = new List<Tile>();
            Tile currentTile;
            
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if (!(x == 0 && y == 0))
                    {
                        var fucker = new Vec(x, y) + position;

                        if (ContainsPosition(fucker))
                        {
                            currentTile = tiles[fucker.X, fucker.Y];

                            if (!currentTile.isBlocked && !currentTile.hasEntity)
                            {
                                adjacents.Add(currentTile);
                            }
                        }
                    }
                }
            }

            return adjacents;
        }

        private Tile[,] tiles;
        public Tile[,] getTileArray
        {
            get
            {
                return tiles;
            }
        }
        public void SetTiles(Tile[,] value)
        {
            tiles = value;
        }

        private List<IEntity> entities;
        public int getEntitiesCount { get { return entities.Count; } }
        public IEntity GetEntity(int index) { return entities[index]; }


        public IEntity getPlayer 
        { 
            get
            {
                for (int iter = 0; iter < entities.Count; iter++)
                    if (entities[iter].IsPlayer)
                        return entities[iter];

                return null;
            } 
        }

        public Vec CameraPosition { get; private set; }


        public SceneImplementation(SceneData data, IMessageLog messages)
        {
            Data = data;
            Messages = messages;
            Time = new SceneTime();
            Messages.sceneTime = Time;
        }

        public void Update()
        {
            SetCameraPosition();
        }

        private void SetCameraPosition(bool forceToPlayer = false)
        {
            if (forceToPlayer)
            {
                CameraPosition = getPlayer.tile.Position;
                return;
            }

            Vec diff = CameraPosition - getPlayer.tile.Position;
            int magnitude = System.Math.Abs(diff.X) + System.Math.Abs(diff.Y);

            if (magnitude > 7)
            {
                var x = 0;
                var y = 0;

                if (getPlayer.tile.Position.X > CameraPosition.X)
                    x += 1;
                if (getPlayer.tile.Position.Y > CameraPosition.Y)
                    y += 1;
                if (getPlayer.tile.Position.X < CameraPosition.X)
                    x -= 1;
                if (getPlayer.tile.Position.Y < CameraPosition.Y)
                    y -= 1;

                CameraPosition = CameraPosition + new Vec(x, y);                
            }
        }

        public void LoadScene()
        {
            FileOperations.LoadMap(this, "FIGHTPIT");
        }


        public void EventTest(object sender, EventArgs e)
        {
            var prefs = CommonGraphics.WINDOW_TEXT_BAD;
            Messages.Add(new CharacterString("player came back from the nightmares...", prefs.ForeColor, prefs.BackColor));
        }

        public void LoadEntities()
        {
            NewEntityList();
            FileOperations.LoadEntitiesFromText(this, entities);
            SetCameraPosition(true);

            getPlayer.health.Resuscitation += new ChangedEventHandler(EventTest);
        }

        public void CreateDefaultEntities()
        {
            NewEntityList();

            IEntity player = new BaseEntity(this);
            player.IsPlayer = true;
            player.Graphic = new Character(Glyph.Face, TermColor.LightGold, TermColor.DarkGray);
            player.BaseType = "PLAYER";
            AddEntity(player, new Vec(7, 7));

            IEntity uke = new BaseEntity(this);
            uke.Graphic = new Character(Glyph.Ampersand, TermColor.LightGold, TermColor.DarkGray);
            uke.BaseType = "NPC";
            AddEntity(uke, new Vec(13, 7));

            SetCameraPosition(true);

            getPlayer.health.Resuscitation += new ChangedEventHandler(EventTest);
        }

        public void NewEntityList()
        {
            entities = new List<IEntity>();
        }

        public void AddEntity(IEntity entity, Vec position)
        {
            var tile = GetTile(position);
            if (tile == null) return;
            if (tile.isBlocked) return;

            tile.SetEntity(entity);
            entities.Add(entity);
        }

        public void RemoveEntity(Vec position)
        {
            var tile = GetTile(position);
            if (tile == null) return;
            if (tile.hasEntity)
            {
                var ent = tile.Entity;
                tile.SetEntity(null);
                entities.Remove(ent);
            }
        }

        public Tile GetTile(Vec position)
        {
            if (!Contains(position))
                return new Tile(Vec.Zero, CommonGraphics.GAME_VOID);

            return tiles[position.X, position.Y];
        }

        private bool Contains(Vec point)
        {
            bool pass = true;

            if (point.X < 0) pass = false;
            if (point.Y < 0) pass = false;
            if (point.X >= Data.Size.X) pass = false;
            if (point.Y >= Data.Size.Y) pass = false;

            return pass;
        }
    }
}
