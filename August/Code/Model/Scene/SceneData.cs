﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public class SceneData
    {
        public string MapName;
        private static string DefaultMapName = "DEFAULT_MAP_NAME";
        public Vec Size;
        private static Vec DefaultSize = new Vec(10, 20);

        public SceneData()
        {
            MapName = DefaultMapName;
            Size = DefaultSize;
        }
    }
}
