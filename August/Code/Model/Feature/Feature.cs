﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace August
{
    public interface IFeature
    {
        string Name { get; set; }
        Character Graphic { get; set; }
        bool blocksMovement { get; set; }
        bool blocksVision { get; set; }
        bool blocksHearing { get; set; }
        bool consumesObjects { get; set; }
        bool isBreakable { get; set; }
        IFeature getBrokenFeature();

        Tile tile { get; set; }
    }

    public class BaseFeature : IFeature
    {
        public string Name { get; set; }
        public Character Graphic { get; set; }
        public bool blocksMovement { get; set; }
        public bool blocksVision { get; set; }
        public bool blocksHearing { get; set; }
        public bool consumesObjects { get; set; }
        public bool isBreakable { get; set; }
        public virtual IFeature getBrokenFeature()
            { return this; }

        public Tile tile { get; set; }


        public BaseFeature()
        {
            Name = "base feature";
            Graphic = new Character('!', TermColor.LightRed);
            blocksMovement = false;
            blocksVision = false;
            blocksHearing = false;
            consumesObjects = false;
            isBreakable = false;
        }
    }

    public class WallFeature : BaseFeature
    {
        public WallFeature()
        {
            Name = "wall";
            Graphic = new Character(Glyph.Dashes, TermColor.Gray, TermColor.DarkGray);
            blocksMovement = true;
            blocksVision = true;
            blocksHearing = true;
        }
    }

    public class FloorFeature : BaseFeature
    {
        public FloorFeature()
        {
            Name = "floor";
            Graphic = new Character('.', TermColor.DarkGray, TermColor.Gray);
            blocksMovement = false;
            blocksVision = false;
            blocksHearing = false;
        }
    }

    public class DoorFeature : BaseFeature
    {
        public DoorFeature()
        {
            Name = "door";
            Graphic = new Character(Glyph.Door, TermColor.Brown, TermColor.DarkGray);
            blocksMovement = false;
            blocksVision = true;
            blocksHearing = true;
        }
    }

    public class TreeFeature : BaseFeature
    {
        public TreeFeature()
        {
            Name = "tree";
            Graphic = new Character(Glyph.TreeConical, TermColor.Green, TermColor.DarkGreen);
            blocksMovement = true;
            blocksVision = true;
            blocksHearing = false;
        }
    }

    public class BushFeature : BaseFeature
    {
        public BushFeature()
        {
            Name = "bush";
            Graphic = new Character(Glyph.Hill, TermColor.Green, TermColor.DarkYellow);
            blocksMovement = false;
            blocksVision = true;
            blocksHearing = false;
            isBreakable = true;            
        }

        public override IFeature getBrokenFeature()
        {
            return new GrassFeature();
        }
    }

    public class WaterFeature : BaseFeature
    {
        public WaterFeature()
        {
            Name = "water";
            Graphic = new Character('~', TermColor.Gray, TermColor.DarkCyan);
            blocksMovement = true;
            blocksVision = false;
            blocksHearing = false;
            consumesObjects = true;
        }
    }

    public class GrassFeature : BaseFeature
    {
        public GrassFeature()
        {
            Name = "grass";
            Graphic = new Character(Glyph.Grass, TermColor.Brown, TermColor.DarkGold);
            blocksMovement = false;
            blocksVision = false;
            blocksHearing = false;
        }
    }

    public class SandFeature : BaseFeature
    {
        public SandFeature()
        {
            Name = "sand";
            Graphic = new Character('.', TermColor.DarkGold, TermColor.Brown);
            blocksMovement = false;
            blocksVision = false;
            blocksHearing = false;
        }
    }

    public class FenceFeature : BaseFeature
    {
        public FenceFeature()
        {
            Name = "fence";
            Graphic = new Character(Glyph.VerticalBars, TermColor.LightBrown, TermColor.Brown);
            blocksMovement = true;
            blocksVision = false;
            blocksHearing = false;
            isBreakable = true;
        }

        public override IFeature getBrokenFeature()
        {
            return new SandFeature();
        }
    }

    public class WindowFeature : BaseFeature
    {
        public WindowFeature()
        {
            Name = "window";
            Graphic = new Character('=', TermColor.Cyan, TermColor.DarkGray);
            blocksMovement = true;
            blocksVision = false;
            blocksHearing = true;
            isBreakable = true;
        }
        public override IFeature getBrokenFeature()
        {
            return new FloorFeature();
        }
    }
}
