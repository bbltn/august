﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;
using Bramble.Core;

namespace August
{
    public class Tile
    {
        public Vec Position { get; private set; }

        public string Surface
        {
            get
            {
                if (hasFeature)
                    return Feature.Name;
                return "ground";
            }
        }
        
        public Character Graphic { get; set; }

        public IFeature Feature { get; private set; }
        public IEntity Entity { get; private set; }

        public bool hasFeature { get { return Feature != null; } }
        public bool hasEntity { get { return Entity != null; } }
        public bool isBlocked
        {
            get
            {
                if (hasEntity)
                    return true;

                if (hasFeature)
                {
                    if (Feature.blocksMovement)
                        return true;
                }

                return false;
            }
        }

        public string Name { get { if (hasFeature) return Feature.Name; else return "ground"; } }

        public bool isBreakable
        {
            get
            {
                if (!hasFeature)
                    return false;

                if (Feature.isBreakable)
                    return true;

                return false;
            }
        }

        public void BreakFeature()
        {
            if (!isBreakable) 
                return;

            Feature = Feature.getBrokenFeature();
        }

        public bool blocksSight
        {
            get
            {
                if (hasFeature)
                    if (Feature.blocksVision)
                        return true;
                return false;
            }
        }

        public bool consumesObjects
        {
            get
            {
                if (hasFeature)
                    if (Feature.consumesObjects)
                        return true;
                return false;
            }
        }

        private void ClearFeature()
        {
            if (Feature != null)
            {
                Feature.tile = null;
                Feature = null;
            }
            return;
        }

        private void ClearEntity()
        {
            if (Entity != null)
            {
                Entity.tile = null;
                Entity = null;
            }
            return;
        }

        public void SetFeature(IFeature feature)
        {
            ClearFeature();
            
            if (feature != null)
            {
                Feature = feature;
                Feature.tile = this;
            }
        }

        public void SetEntity(IEntity entity)
        {
            ClearEntity();

            if (entity != null)
            {
                Entity = entity;
                Entity.tile = this;
            }
        }


        public Tile(Vec position, Character graphic, IFeature feature = null, IEntity entity = null)
        {
            Position = position;
            Graphic = graphic;
            SetFeature(feature);
            SetEntity(entity);
        }

    }
}
