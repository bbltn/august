﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace August
{
    public interface IMessageLog
    {
        CharacterString Get(int index);
        void Add(string message);
        void Add(CharacterString message);
        void Clear();
        int Count { get; }
        ITime sceneTime { get; set; }
    }

    public class MessageLog : IMessageLog
    {
        public ITime sceneTime { get; set; }
        private CharacterString[] Messages;
        private int Index;
        public int Count { get { return Messages.Count(); } }

        public MessageLog(int max = 37)
        {
            Messages = new CharacterString[max];
            Clear();
            Index = 0;
        }

        public CharacterString Get(int index)
        {
            int modifiedIndex = Index + index;
            int max = Count;

            while (modifiedIndex >= max)
                modifiedIndex -= max;

            while (modifiedIndex < 0)
                modifiedIndex += max;

            return Messages[modifiedIndex];
        }

        public void Clear()
        {
            for (int iter = 0; iter < Count; iter++)
                Messages[iter] = new CharacterString(" ", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor);
        }

        public void Add(string message)
        {
            if (sceneTime != null)
            {
                //Add(new CharacterString(TimeUtils.UnitsToString(sceneTime.Current, abbreviate: true)));
                Add(new CharacterString(TimeUtils.UnitsToString(sceneTime.Current, abbreviate: true) + "  " + message, CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
                //Add(new CharacterString("     " + message));
            }
            else
                Add(new CharacterString(message));
            //Add(new CharacterString(""));
        }

        public void Add(CharacterString message)
        {
            IncrementIndex();
            Messages[Index] = message;            

            //IncrementIndex();
            //Messages[Index] = new CharacterString("");
        }

        private void IncrementIndex()
        {
            Index++;
            if (Index >= Count)
                Index = 0;
        }
    }
}
