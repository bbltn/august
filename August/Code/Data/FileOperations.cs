﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    public static class FileOperations
    {
        private const string DATA_PATH = "Data/GameData.txt";
        private const string SAVE_PATH = "Data/Save.txt";

        public static void Save(IScene scene)
        {
            List<string> saveLines = new List<string>();

            WriteSceneToText(scene, saveLines);

            System.IO.File.WriteAllLines(SAVE_PATH, saveLines);
        }

        private static void WriteSceneToText(IScene scene, List<string> lines)
        {
            List<IEntity> entitiesToAdd = new List<IEntity>();

            lines.Add("MAP " + scene.Data.MapName + System.Environment.NewLine);
            for (int y = 0; y < scene.Data.Size.Y; y++)
            {
                lines.Add("");

                for (int x = 0; x < scene.Data.Size.X; x++)
                {
                    if (scene.getTileArray[x,y].hasEntity)
                        entitiesToAdd.Add(scene.getTileArray[x,y].Entity);

                    if (scene.getTileArray[x, y].hasFeature)
                    {
                        if (scene.getTileArray[x, y].Feature.GetType() == typeof(WallFeature))
                            lines[y] = lines[y] + "#";                            
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(WaterFeature))
                            lines[y] = lines[y] + "~";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(GrassFeature))
                            lines[y] = lines[y] + ",";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(FloorFeature))
                            lines[y] = lines[y] + "_";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(DoorFeature))
                            lines[y] = lines[y] + "+";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(TreeFeature))
                            lines[y] = lines[y] + "^";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(SandFeature))
                            lines[y] = lines[y] + ";";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(FenceFeature))
                            lines[y] = lines[y] + "]";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(BushFeature))
                            lines[y] = lines[y] + "*";
                        else if (scene.getTileArray[x, y].Feature.GetType() == typeof(WindowFeature))
                            lines[y] = lines[y] + "=";                      
                    }

                    else
                        lines[y] = lines[y] + '.';
                }
            }
            lines.RemoveAt(lines.Count-1);
            lines.Add("END MAP" + System.Environment.NewLine + System.Environment.NewLine);

            foreach (var thing in entitiesToAdd) 
                WriteEntityToText(thing, lines);
        }

        private static void WriteEntityToText(IEntity entity, List<string> lines)
        {
            lines.Add("ENTITY " + entity.BaseType);
            lines.Add("Glyph:" + entity.Graphic.Glyph.ToString());
            lines.Add("Position:" + entity.tile.Position.X + "," + entity.tile.Position.Y);
            lines.Add("Player:" + entity.IsPlayer.ToString());
            lines.Add("HP:" + HPToString(entity.health));
            lines.Add("KO:" + KOToString(entity.health));
            lines.Add("END ENTITY" + System.Environment.NewLine + System.Environment.NewLine);
        }

        private static string HPToString(IHealth health)
        {
            return health.curHP.ToString() + "/" + health.maxHP.ToString();
        }
        private static string KOToString(IHealth health)
        {
            return health.curKO.ToString() + "/" + health.maxKO.ToString();
        }
        private static int FirstIntFromStringPair(string toParse)
        {
            string segment = "";
            for (int iter = 0; iter < toParse.Count(); iter++)
            {
                if (toParse[iter] == '/')
                    return int.Parse(segment);
                segment += toParse[iter];
            }
            return int.Parse(segment);
        }
        private static int SecondIntFromStringPair(string toParse)
        {
            string segment = "";
            bool record = false;
            for (int iter = 0; iter < toParse.Count(); iter++)
            {
                if (record)
                    segment += toParse[iter];
                if (toParse[iter] == '/')
                    record = true;
            }
            return int.Parse(segment);
        }


        public static void LoadEntitiesFromText(IScene scene, List<IEntity> entities)
        {
            int entityIndex = 0;

            while (true)
            {

                string[] lines = LoadElement("ENTITY", entityIndex);
                if (lines.Length <= 0)
                    return;


                IEntity entity = new BaseEntity(scene);

                string key = "";
                string value = "";
                bool isSettingKey = true;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Glyph":
                            entity.Graphic = new Character(StringToGlyph(value), TermColor.LightGold, TermColor.DarkGray);
                            break;

                        case "Position":
                            var position = StringToVec(value);
                            var tile = scene.GetTile(position);
                            tile.SetEntity(entity);
                            break;

                        case "Player":
                            var state = StringToBool(value);
                            entity.IsPlayer = state;
                            break;

                        case "HP":
                            entity.health.maxHP = SecondIntFromStringPair(value);
                            entity.health.SetHP(FirstIntFromStringPair(value));
                            break;

                        case "KO":
                            entity.health.maxKO = SecondIntFromStringPair(value);
                            entity.health.SetKO(FirstIntFromStringPair(value));
                            break;

                        default:
                            scene.Messages.Add("ERROR: Entity Data failed to load");
                            scene.Messages.Add("data key:   " + key);
                            scene.Messages.Add("data value: " + value);
                            break;
                    }

                    if (entity.IsPlayer)
                        entity.BaseType = "PLAYER";
                    else
                        entity.BaseType = "NPC";
                }

                entities.Add(entity);
                entityIndex++;
            }
        }

        public static void LoadMap(IScene scene, string MapName)
        {
            // get the map
            var lines = LoadElement("MAP", MapName);

            //get dimensions
            var x = lines[0].Length;
            var y = lines.Length;
            
            //set data stuff
            scene.Data.MapName = MapName;
            scene.Data.Size.X = x;
            scene.Data.Size.Y = y;

            // create the tile array 
            Tile[,] tiles = new Tile[x, y];

            // configure the tiles            
            for (int iterX = 0; iterX < x; iterX++)
            {
                for (int iterY = 0; iterY < y; iterY++)
                {
                    tiles[iterX, iterY] = new Tile(new Vec(iterX, iterY), new Malison.Core.Character('.', TermColor.Brown, TermColor.DarkBrown));

                    tiles[iterX, iterY].SetFeature(CharToFeature(lines[iterY][iterX]));

                    switch (lines[iterY][iterX])
                    {
                        case '#':
                            tiles[iterX, iterY].SetFeature(new WallFeature());
                            break;
                        case '~':
                            tiles[iterX, iterY].SetFeature(new WaterFeature());
                            break;
                        case ',':
                            tiles[iterX, iterY].SetFeature(new GrassFeature());
                            break;
                        case '_':
                            tiles[iterX, iterY].SetFeature(new FloorFeature());
                            break;
                        case '+':
                            tiles[iterX, iterY].SetFeature(new DoorFeature());
                            break;
                        case '^':
                            tiles[iterX, iterY].SetFeature(new TreeFeature());
                            break;
                        case ';':
                            tiles[iterX, iterY].SetFeature(new SandFeature());
                            break;
                        case ']':
                            tiles[iterX, iterY].SetFeature(new FenceFeature());
                            break;
                        case '*':
                            tiles[iterX, iterY].SetFeature(new BushFeature());
                            break;
                        case '=':
                            tiles[iterX, iterY].SetFeature(new WindowFeature());
                            break;
                        default:
                            break;
                    }        
                }
            }             

            scene.SetTiles(tiles);
        }

        public static BaseFeature CharToFeature(char value)
        {
            switch (value)
            {
                case '#':
                    return new WallFeature();
                    
                case '~':
                    return new WaterFeature();
                    
                case ',':
                    return new GrassFeature();
                    
                case '_':
                    return new FloorFeature();
                    
                case '+':
                    return new DoorFeature();
                    
                case '^':
                    return new TreeFeature();
                    
                case ';':
                    return new SandFeature();
                    
                case ']':
                    return new FenceFeature();
                    
                case '*':
                    return new BushFeature();

                case '=':
                    return new WindowFeature();

                default:
                    return null;
            }
        }

        private static string[] LoadElement(string ElementType, int Instance)
        {

            var file = System.IO.File.ReadAllLines(SAVE_PATH);

            List<string> lines = new List<string>();
            bool isTargetLine = false;
            int instanceCounter = 0;
            for (int currentLine = 0; currentLine < file.Length; currentLine++)
            {
                if (file[currentLine] == "END " + ElementType)
                    isTargetLine = false;

                if (isTargetLine) lines.Add(file[currentLine]);

                if (file[currentLine].Contains(ElementType) && file[currentLine] != "END " + ElementType)
                {
                    if (instanceCounter == Instance)
                        isTargetLine = true;

                    instanceCounter++;
                }
            }

            return lines.ToArray();
        }

        private static string[] LoadElement(string ElementType, string Name)
        {
            var file = System.IO.File.ReadAllLines(SAVE_PATH);

            List<string> lines = new List<string>();
            bool isTargetLine = false;

            for (int iter = 0; iter < file.Length; iter++)
            {
                if (file[iter] == "END " + ElementType)
                    isTargetLine = false;

                if (isTargetLine) lines.Add(file[iter]);

                if (file[iter] == ElementType + " " + Name)
                    isTargetLine = true;
            }

            return lines.ToArray();
        }

        private static Glyph StringToGlyph(string value)
        {
            switch (value)
            {
                case "Face":
                    return Glyph.Face;

                case "Ampersand":
                    return Glyph.Ampersand;

                default:
                    return Glyph.Backslash;
            }
        }

        private static bool StringToBool(string value)
        {
            if (value == "True" || value == "true") 
                return true;
            else if (value == "1" || (value == "One" || value == "one")) 
                return true;
            else if (value == "Yes" || value == "yes")
                return true;
            else if (value == "On" || value == "on")
                return true;

            return false;
        }

        private static Vec StringToVec(string value)
        {
            Vec answer = Vec.Zero;

            string first = "";
            string second = "";
            bool hasPassedComma = false;

            for (int x = 0; x < value.Length; x++)
            {
                if (value[x] == ',')
                    hasPassedComma = true;

                else if (!hasPassedComma)
                    first += value[x];

                else
                    second += value[x];
            }

            answer.X = int.Parse(first);
            answer.Y = int.Parse(second);

            return answer;
        }
    }
}
