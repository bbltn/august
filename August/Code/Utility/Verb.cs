﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace August
{
    public class Verb
    {
        public string Abstract { get; private set; }
        public string Past { get; private set; }
        public string Present { get; private set; }
        public string Future { get; private set; }

        public Verb
            (string abstr = "undefined_abstract_verb",
            string past = "undefined_abstract_verb",
            string present = "undefined_abstract_verb",
            string future = "undefined_abstract_verb")
        {
            Abstract = abstr;
            Past = past;
            Present = present;
            Future = future;
        }
    }
}
