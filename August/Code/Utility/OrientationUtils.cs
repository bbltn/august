﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public enum CardinalOrientation { North, Northeast, East, Southeast, South, Southwest, West, Northwest, Colocational }

    public static class OrientationUtils
    {
        public static string Name(CardinalOrientation orientation, bool abbreviate = false, bool capslock = false, bool capitalize = false)
        {
            switch (orientation)
            {
                case CardinalOrientation.North:
                    if (abbreviate && (capslock || capitalize)) 
                        return "N";
                    if (abbreviate && !capslock && !capitalize) 
                        return "n";
                    if (!abbreviate && capslock)
                        return "NORTH";
                    if (!abbreviate && capitalize)
                        return "North";
                    return "north";

                case CardinalOrientation.Northeast:
                    if (abbreviate && (capslock || capitalize))
                        return "NE";
                    if (abbreviate && !capslock && !capitalize)
                        return "ne";
                    if (!abbreviate && capslock)
                        return "NORTHEAST";
                    if (!abbreviate && capitalize)
                        return "Northeast";
                    return "northeast";

                case CardinalOrientation.East:
                    if (abbreviate && (capslock || capitalize))
                        return "E";
                    if (abbreviate && !capslock && !capitalize)
                        return "e";
                    if (!abbreviate && capslock)
                        return "EAST";
                    if (!abbreviate && capitalize)
                        return "East";
                    return "east";

                case CardinalOrientation.Southeast:
                    if (abbreviate && (capslock || capitalize))
                        return "SE";
                    if (abbreviate && !capslock && !capitalize)
                        return "se";
                    if (!abbreviate && capslock)
                        return "SOUTHEAST";
                    if (!abbreviate && capitalize)
                        return "Southeast";
                    return "southeast";

                case CardinalOrientation.South:
                    if (abbreviate && (capslock || capitalize))
                        return "S";
                    if (abbreviate && !capslock && !capitalize)
                        return "s";
                    if (!abbreviate && capslock)
                        return "SOUTH";
                    if (!abbreviate && capitalize)
                        return "South";
                    return "south";

                case CardinalOrientation.Southwest:
                    if (abbreviate && (capslock || capitalize))
                        return "SW";
                    if (abbreviate && !capslock && !capitalize)
                        return "sw";
                    if (!abbreviate && capslock)
                        return "SOUTHWEST";
                    if (!abbreviate && capitalize)
                        return "Southwest";
                    return "southwest";

                case CardinalOrientation.West:
                    if (abbreviate && (capslock || capitalize))
                        return "W";
                    if (abbreviate && !capslock && !capitalize)
                        return "w";
                    if (!abbreviate && capslock)
                        return "WEST";
                    if (!abbreviate && capitalize)
                        return "West";
                    return "west";

                case CardinalOrientation.Northwest:
                    if (abbreviate && (capslock || capitalize))
                        return "NW";
                    if (abbreviate && !capslock && !capitalize)
                        return "nw";
                    if (!abbreviate && capslock)
                        return "NORTHWEST";
                    if (!abbreviate && capitalize)
                        return "Northwest";
                    return "northwest";

                case CardinalOrientation.Colocational:
                    return "==";
            }

            return "?";
        }

        public static CardinalOrientation Cardinal(Vec observer, Vec target)
        {
            Vec slope = VecUtils.Normalize(target - observer);
            //Vec slope = target - observer;

            Vec testVec = new Vec(0, -1);
            float bestScore = VecUtils.Distance(slope, testVec);
            CardinalOrientation direction = CardinalOrientation.North;

            testVec = new Vec(1, -1);
            if (VecUtils.Distance(slope, testVec) < bestScore)
            {
                bestScore = VecUtils.Distance(slope, testVec);
                direction = CardinalOrientation.Northeast;
            }

            testVec = new Vec(1, 0);
            if (VecUtils.Distance(slope, testVec) < bestScore)
            {
                bestScore = VecUtils.Distance(slope, testVec);
                direction = CardinalOrientation.East;
            }

            testVec = new Vec(1, 1);
            if (VecUtils.Distance(slope, testVec) < bestScore)
            {
                bestScore = VecUtils.Distance(slope, testVec);
                direction = CardinalOrientation.Southeast;
            }

            testVec = new Vec(0, 1);
            if (VecUtils.Distance(slope, testVec) < bestScore)
            {
                bestScore = VecUtils.Distance(slope, testVec);
                direction = CardinalOrientation.South;
            }

            testVec = new Vec(-1, 1);
            if (VecUtils.Distance(slope, testVec) < bestScore)
            {
                bestScore = VecUtils.Distance(slope, testVec);
                direction = CardinalOrientation.Southwest;
            }

            testVec = new Vec(-1, 0);
            if (VecUtils.Distance(slope, testVec) < bestScore)
            {
                bestScore = VecUtils.Distance(slope, testVec);
                direction = CardinalOrientation.West;
            }

            testVec = new Vec(-1, -1);
            if (VecUtils.Distance(slope, testVec) < bestScore)
            {
                bestScore = VecUtils.Distance(slope, testVec);
                direction = CardinalOrientation.Northwest;
            }

            //return CardinalOrientation.Colocational;

            return direction;
        }
    }
}
