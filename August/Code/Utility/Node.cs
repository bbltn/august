﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{

    public interface INode
    {
        Vec Transform { get; set; }
        Node Parent { get; set; }

        int MoveDistance { get; }
    }

    public class Node : INode
    {
        public Vec Transform { get; set; }
        public Node Parent { get; set; }

        public int MoveDistance
        {
            get
            {
                if (Parent == null) return 1;
                else return Parent.MoveDistance + 1;
            }
        }


        public Node(Vec newTransform, Node cameFrom)
        {
            Transform = newTransform;
            Parent = cameFrom;
        }

    }
}