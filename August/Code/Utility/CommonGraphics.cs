﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace August
{
    static class CommonGraphics
    {
        public static Character GAME_VOID
        {
            get { return new Character(Glyph.GrayFill, TermColor.DarkGray, TermColor.DarkGold); }
        }
        public static Character APP_VOID
        {
            get { return new Character(Glyph.GrayFill, TermColor.Black, TermColor.DarkGray); }
        }
        public static Character WINDOW_VOID
        {
            get { return new Character(Glyph.Space, TermColor.DarkGray, TermColor.DarkGray); }
        }

        public static Character WINDOW_TEXT_STANDARD
        {
            get { return new Character(Glyph.Space, TermColor.LightGray, TermColor.DarkGray); }
        }
        public static Character WINDOW_TEXT_BOLD
        {
            get { return new Character(Glyph.Space, TermColor.White, TermColor.DarkGray); }
        }
        public static Character WINDOW_TEXT_HIGHLIGHT
        {
            get { return new Character(Glyph.Space, TermColor.LightGray, TermColor.Yellow); }
        }
        public static Character WINDOW_TEXT_BAD
        {
            get { return new Character(Glyph.Space, TermColor.White, TermColor.DarkRed); }
        }
        public static Character WINDOW_TEXT_GOOD
        {
            get { return new Character(Glyph.Space, TermColor.White, TermColor.DarkGreen); }
        }
    }
}
