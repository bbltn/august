﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public static class Shape
    {
        public static List<Vec> CreateLine(Vec first, Vec second)
        {
            System.Diagnostics.Debug.Assert(first != null);
            if (first == null) return new List<Vec>();

            System.Diagnostics.Debug.Assert(second != null);
            if (second == null) return new List<Vec>();


            List<Vec> line = new List<Vec>();

            bool steep = Math.Abs(second.Y - first.Y) > Math.Abs(second.X - first.X);

            if (steep)
            {
                first = new Vec(first.Y, first.X);
                second = new Vec(second.Y, second.X);
            }

            if (first.X > second.X)
            {
                Vec swap = new Vec(first.X, first.Y);

                first = new Vec(second.X, second.Y);
                second = new Vec(swap.X, swap.Y);
            }

            Vec delta = new Vec(second.X - first.X, Math.Abs(second.Y - first.Y));

            int err = (delta.X / 2);
            int ystep = (first.Y < second.Y ? 1 : -1);
            int y = first.Y;

            for (int x = first.X; x <= second.X; x++)
            {
                if (steep) line.Add(new Vec(y, x));
                else line.Add(new Vec(x, y));

                err = err - delta.Y;
                if (err < 0)
                {
                    y += ystep;
                    err += delta.X;
                }
            }

            return line;
        }
    }
}
