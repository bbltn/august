﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public static class Navigation
    {
        static int QUALITY = 250;

        public static List<Vec> CreatePath(Model model, Vec pathStart, Vec pathTarget)
        {
            List<Node> OpenNodes = new List<Node>();
            List<Node> ClosedNodes = new List<Node>();

            OpenNodes.Add(new Node(pathStart, null));

            Node currentLowestNode = GetLowestCostOpenNode(pathTarget, OpenNodes);

            int iterations = 0;
            int maxIterations = QUALITY;
            while (GetLowestCostOpenNode(pathTarget, OpenNodes) != null && currentLowestNode.Transform != pathTarget && iterations < maxIterations)
            {
                currentLowestNode = GetLowestCostOpenNode(pathTarget, OpenNodes);

                if (currentLowestNode == null)
                    return null;

                CloseNode(ref OpenNodes, ref ClosedNodes, currentLowestNode);

                var neighbors = NeighborsOfNode(model, currentLowestNode);

                foreach (Node neighbor in neighbors)
                {
                    if (!NodeAlreadyClosed(ClosedNodes, neighbor))
                        OpenNode(ref OpenNodes, neighbor);
                }

                if (OpenNodes.Count <= 0) return null;

                iterations++;
            }

            List<Vec> path = new List<Vec>();

            path.Insert(0, currentLowestNode.Transform);
            var nextPathNode = currentLowestNode.Parent;

            if (nextPathNode == null) return null;

            while (nextPathNode.Parent != null)
            {
                path.Insert(0, nextPathNode.Transform);

                nextPathNode = nextPathNode.Parent;
            }



            return path;
        }



        public static Node GetLowestCostOpenNode(Vec pathTarget, List<Node> OpenNodes)
        {
            if (OpenNodes.Count <= 0) return null;

            int lowestCostNodeIndex = 0;

            for (int iter = 0; iter < OpenNodes.Count; iter++)
            {
                if (Cost(pathTarget, OpenNodes[iter]) < Cost(pathTarget, OpenNodes[lowestCostNodeIndex]))
                    lowestCostNodeIndex = iter;
            }

            return OpenNodes[lowestCostNodeIndex];
        }






        public static List<Node> NeighborsOfNode(Model model, Node origin)
        {
            System.Diagnostics.Debug.Assert(model != null);
            if (model == null) return new List<Node>();

            System.Diagnostics.Debug.Assert(origin != null);
            if (origin == null) return new List<Node>();



            List<Node> neighbors = new List<Node>();
            Vec newTransform = Vec.Zero;
            bool addNeighbor = true;

            for (int iterX = -1; iterX <= 1; iterX++)
            {
                for (int iterY = -1; iterY <= 1; iterY++)
                {
                    if (iterX == 0 && iterY == 0) { }
                    else
                    {

                        newTransform = new Vec(origin.Transform.X + iterX, origin.Transform.Y + iterY);
                        addNeighbor = true;

                        if (model.Scene.ContainsPosition(newTransform) == false) 
                            addNeighbor = false;
                        
                        else if (model.Scene.GetTile(newTransform).hasFeature)
                            if (model.Scene.GetTile(newTransform).Feature.blocksMovement) 
                                addNeighbor = false;                        

                        //if (model.Scene.GetTile(newTransform).hasEntity)
                            //addNeighbor = false;

                        if (addNeighbor)
                            neighbors.Add(new Node(newTransform, origin));
                    }
                }
            }

            return neighbors;
        }

        public static bool NodeAlreadyClosed(List<Node> ClosedNodes, Node node)
        {
            System.Diagnostics.Debug.Assert(node != null);
            if (node == null) return false;

            if (ClosedNodes == null) return false;

            foreach (Node test in ClosedNodes)
            {
                if (test.Transform == node.Transform) return true;
            }

            return false;
        }

        public static bool NodeAlreadyOpen(ref List<Node> OpenNodes, Node node)
        {
            System.Diagnostics.Debug.Assert(node != null);
            if (node == null) return false;

            if (OpenNodes == null) return false;

            foreach (Node test in OpenNodes)
            {
                if (test.Transform == node.Transform) return true;
            }

            return false;
        }

        public static void CloseNode(ref List<Node> OpenNodes, ref List<Node> ClosedNodes, Node node)
        {
            System.Diagnostics.Debug.Assert(node != null);
            if (node == null) return;


            System.Diagnostics.Debug.Assert(ClosedNodes != null);
            if (ClosedNodes == null) return;

            if (OpenNodes != null)
            {
                foreach (Node test in OpenNodes)
                {
                    if (test.Transform == node.Transform)
                    {
                        OpenNodes.Remove(test);
                        break;
                    }
                }
            }

            ClosedNodes.Insert(0, node);
        }

        public static void OpenNode(ref List<Node> OpenNodes, Node node)
        {
            OpenNodes.Insert(0, node);
        }


        public static float Cost(Vec pathTarget, Node node)
        {
            System.Diagnostics.Debug.Assert(node != null);
            if (node == null) return 0;

            if (pathTarget == null) return node.MoveDistance;

            else return node.MoveDistance + VecUtils.Distance(node.Transform, pathTarget);
        }
    }
}
