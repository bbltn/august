﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public interface IInputProcessor
    {
        void Register(char KeyChar);
        bool hasInput { get; }
        BaseCommand PullCommand();
    }
    
    public class InputProcessor : IInputProcessor
    {
        private List<char> keysPressed;
        public bool hasInput
        {
            get { return keysPressed.Count > 0; }
        }

        public InputProcessor()
        {
            keysPressed = new List<char>();
        }

        public BaseCommand PullCommand()
        {
            if (hasInput)
            {
                var command = KeyToCommand(keysPressed[0]);
                keysPressed.RemoveAt(0);
                return command;
            }

            else
                return new BaseCommand();
        }

        static private BaseCommand KeyToCommand(char key)
        {
            switch (key)
            {
                case '1':
                    return new VectorCommand(new Vec(-1, 1));
                case '2':
                    return new VectorCommand(new Vec(0, 1));
                case '3':
                    return new VectorCommand(new Vec(1, 1));
                case '4':
                    return new VectorCommand(new Vec(-1, 0));
                case '6':
                    return new VectorCommand(new Vec(1, 0));
                case '7':
                    return new VectorCommand(new Vec(-1, -1));
                case '8':
                    return new VectorCommand(new Vec(0, -1));
                case '9':
                    return new VectorCommand(new Vec(1, -1));

                default:
                    return new LetterCommand(key);
            }
        }

        public void Register(char KeyChar)
        {
            keysPressed.Add(KeyChar);
        }        
      
    }
}
