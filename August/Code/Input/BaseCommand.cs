﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace August
{
    public class BaseCommand
    {
        public BaseCommand()
        {
        }
    }

    public class VectorCommand : BaseCommand
    {
        public Vec Vector;

        public VectorCommand(Vec newVector) : base()
        {
            Vector = newVector;
        }
    }

    public class LetterCommand : BaseCommand
    {
        public char Letter;

        public LetterCommand(char newLetter)
            : base()
        {
            Letter = newLetter;
        }
    }

    public class NumberCommand : BaseCommand
    {
        public int Number;

        public NumberCommand(int newNumber)
            : base()
        {
            Number = newNumber;
        }
    }
}
