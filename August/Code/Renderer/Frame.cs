﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    public interface IFrame
    {
        void Create(Vec size);
        Character CharAtPosition(Vec position);
        void Set(Vec position, Character newChar);
        void Write(Vec position, CharacterString sentence);
        void Fill(Character value);
        Vec Size { get; }
    }

    public class Frame : IFrame
    {
        private Character[,] Chars;
        public Vec Size { get; private set; }

        public Frame(Vec size)
        {
            if (size == null) 
                Create(Vec.One);
            else
                Create(size);
        }
        public void Create(Vec size)
        {
            Size = size;
            Chars = new Character[Size.X, Size.Y];
            Fill(new Character(' '));
        }


        public Character CharAtPosition(Vec position)
        {
            if (Contains(position))
                return Chars[position.X, position.Y];
            else 
                return new Character(' ');
        }


        public void Set(Vec position, Character newChar)
        {
            if (Contains(position))
                Chars[position.X, position.Y] = newChar;
        }

        public void Write(Vec position, CharacterString sentence)
        {
            Vec modifiedPosition = position;
            for (int iter = 0; iter < sentence.Count; iter++)
            {
                modifiedPosition.X = position.X + iter;
                Set(modifiedPosition, sentence[iter]);
            }
        }

        public void Fill(Character value)
        {
            for (int x = 0; x < Size.X; x++)
            {
                for (int y = 0; y < Size.Y; y++)
                {
                    Chars[x, y] = value;
                }
            }
        }

        private bool Contains(Vec point)
        {
            bool pass = true;

            if (point.X < 0) pass = false;
            if (point.Y < 0) pass = false;
            if (point.X >= Size.X) pass = false;
            if (point.Y >= Size.Y) pass = false;

            return pass;
        }

    }
}
