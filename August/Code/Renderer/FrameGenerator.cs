﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    public class FrameGenerator
    {
        private IFrame LastFrame;
        private IFrame NextFrame;

        private List<IWindow> Windows;
        
        private List<Vec> ChangedPoints = new List<Vec>();
        public List<Vec> GetChangedPoints() { return ChangedPoints; }

        //private static Character DEFAULT_CHARACTER = new Character(' ', TermColor.White, TermColor.Black);

        public FrameGenerator(Vec size)
        {
            SetRenderSize(size);
            Windows = new List<IWindow>();
            AddWindows();
        }

        private void AddWindows()
        {
            Windows.Add(new GameWindow(origin: new Vec(21, 4), size: new Vec(81, 40)));
            Windows.Add(new ConditionsWindow(origin: new Vec(104, 3), size: new Vec(16, 4)));
            Windows.Add(new MessageLogWindow(origin: new Vec(104, 8), size: new Vec(55, 37)));
            Windows.Add(new AlertPhaseWindow(origin: new Vec(21, 3), size: new Vec(81, 1)));
            Windows.Add(new AlertPhaseWindow(origin: new Vec(21, 44), size: new Vec(81, 1), showText: false));
            Windows.Add(new AlertPhaseWindow(origin: new Vec(20, 3), size: new Vec(1, 42), showText: false));
            Windows.Add(new AlertPhaseWindow(origin: new Vec(102, 3), size: new Vec(1, 42), showText: false));
            //Windows.Add(new EnergyWindow(origin: new Vec(20, 55), size: new Vec(100, 10)));
            Windows.Add(new ControlWindow(origin: new Vec(20, 46), size: new Vec(25, 10)));
            Windows.Add(new EntityWindow(origin: new Vec(1, 3), size: new Vec(18, 28)));
            Windows.Add(new KnownEntityWindow(origin: new Vec(1, 32), size: new Vec(18, 10)));
            //Windows.Add(new TileWindow(origin: new Vec(123, 10), size: new Vec(18, 6)));
            
        }

        public void Update(Model model)
        {
            CreateNextFrame(model);
            UpdateChangedPoints();
            ApplyChangedPoints();
        }

        private void ApplyChangedPoints()
        {
            foreach (Vec changedPoint in ChangedPoints)
            {
                LastFrame.Set(changedPoint, NextFrame.CharAtPosition(changedPoint));
            }
        }

        private void CreateNextFrame(Model model)
        {
            NextFrame.Fill(CommonGraphics.APP_VOID);

            foreach (IWindow window in Windows)
            {
                window.Update(model);
                if (window.isVisible)
                    DrawWindowToNextFrame(window);
            }
        }

        private void DrawWindowToNextFrame(IWindow window)
        {
            Vec position = Vec.Zero;

            for (int x = 0; x < window.Size.X; x++)
            {
                for (int y = 0; y < window.Size.Y; y++)
                {
                    position.X = x;
                    position.Y = y;

                    var thing =  window.frame.CharAtPosition(position);

                    if (thing.IsWhitespace)
                    {
                        if (!window.isTransparent)
                            NextFrame.Set(window.Origin + position, thing);
                    }

                    else
                           NextFrame.Set(window.Origin + position, thing);
                }
            }
        }



        public Character CharAtPosition(Vec position) 
        {
            return LastFrame.CharAtPosition(position);
        }
         
         
        
        private void UpdateChangedPoints()
        {
            ChangedPoints.Clear();
            Vec Size = NextFrame.Size;
            Vec point = Vec.Zero;
            for (int x = 0; x < Size.X; x++)
            {
                for (int y = 0; y < Size.Y; y++)
                {
                    point.X = x;
                    point.Y = y;

                    if (LastFrame.CharAtPosition(point).Equals(NextFrame.CharAtPosition(point)) == false)
                    {
                        ChangedPoints.Add(point);
                    }
                }
            }
        }
        

        private void SetRenderSize(Vec Size)
        {
            LastFrame = new Frame(Size);
            NextFrame = new Frame(Size);
        }
    }
}
