﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    class AlertPhaseWindow : Window
    {
        public bool ShowText { get; set; }

        public AlertPhaseWindow(Vec origin, Vec size, bool showText = true)
            : base(origin, size)
        {
            ShowText = showText;
        }

        public override void Update(Model model)
        {
            string toPrint;

            switch (model.Scene.getPlayer.knowledge.Alarm)
            {
                case AlarmLevel.Combat:
                    frame.Fill(new Character(Glyph.Space, TermColor.Red, TermColor.Red));
                    if (ShowText)
                    {
                        toPrint = "COMBAT";
                        frame.Write(new Vec((Size.X / 2) - (toPrint.Length / 2), (Size.Y / 2)), new CharacterString(toPrint, TermColor.LightGray, TermColor.Red));
                    }
                    break;

                case AlarmLevel.Alert:
                    frame.Fill(new Character(Glyph.Space, TermColor.Orange, TermColor.Orange));
                    if (ShowText)
                    {
                        toPrint = "ALERT";
                        frame.Write(new Vec((Size.X / 2) - (toPrint.Length / 2), (Size.Y / 2)), new CharacterString(toPrint, TermColor.White, TermColor.Orange));
                    }
                    break;

                case AlarmLevel.Suspect:
                    frame.Fill(new Character(Glyph.Space, TermColor.Yellow, TermColor.Yellow));
                    if (ShowText)
                    {
                        toPrint = "SUSPICION";
                        frame.Write(new Vec((Size.X / 2) - (toPrint.Length / 2), (Size.Y / 2)), new CharacterString(toPrint, TermColor.DarkGray, TermColor.Yellow));
                    }
                    break;

                default:
                    frame.Fill(new Character(Glyph.Space, TermColor.DarkGray, TermColor.DarkGray));
                    if (ShowText)
                    {
                        toPrint = "CALM";
                        frame.Write(new Vec((Size.X / 2) - (toPrint.Length / 2), (Size.Y / 2)), new CharacterString(toPrint, TermColor.LightGray, TermColor.DarkGray));
                    }
                    break;
            }
        }
    }
}
