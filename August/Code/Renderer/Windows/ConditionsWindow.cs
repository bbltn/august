﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    class ConditionsWindow : Window
    {
        public ConditionsWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(Model model)
        {
            base.Update(model);

            List<CharacterString> messagesToPrint = new List<CharacterString>();

            messagesToPrint.Add(new CharacterString(" ", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Time: " + TimeUtils.UnitsToString(model.Scene.Time.Current), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString(TimeUtils.GetTimeOfDay(model.Scene.Time.Current).ToString(), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));

            for (int x = 0; x < messagesToPrint.Count; x++)
                frame.Write(new Vec(0, x), messagesToPrint[x]);

        }
    }
}
