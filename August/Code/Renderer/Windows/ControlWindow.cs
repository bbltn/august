﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    class ControlWindow : Window
    {
        public ControlWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(Model model)
        {
            base.Update(model);

            List<CharacterString> messagesToPrint = new List<CharacterString>();

            messagesToPrint.Add(new CharacterString("Movement: NUMPAD", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Punch: Move into enemy", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Takedown: a", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Push: d", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Sweep: f", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Debug Restart: r", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Debug Health: -/=", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Debug Knockout: _/+", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));

            for (int x = 0; x < messagesToPrint.Count; x++)
                frame.Write(new Vec(0, x), messagesToPrint[x]);

        }
    }
}
