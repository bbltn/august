﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    class KnownEntityWindow : Window
    {
        public KnownEntityWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(Model model)
        {
            base.Update(model);

            var entity = model.Scene.getPlayer;
            List<CharacterString> messagesToPrint = new List<CharacterString>();

            foreach (var ent in model.Scene.getPlayer.knowledge.Enemies)
            {
                AppendEntity(ent, messagesToPrint, model);
                AppendBreakline(messagesToPrint);
            }
            
            for (int x = 0; x < messagesToPrint.Count; x++)
                frame.Write(new Vec(0, x), messagesToPrint[x]);

        }

        private static void AppendEntity(IEntity entity, List<CharacterString> messagesToPrint, Model model)
        {
            TermColor frontCol = TermColor.White;
            TermColor backCol = TermColor.Red;

            string bookend = " ";
            string space = " ";
            
            string distance = "???";
            string direction = "???";
            if (model.Scene.getPlayer.tile != null && entity.tile != null)
            {
                distance = ((int)VecUtils.Distance(model.Scene.getPlayer.tile.Position, entity.tile.Position)).ToString() + "m";
                direction = OrientationUtils.Name(OrientationUtils.Cardinal(model.Scene.getPlayer.tile.Position, entity.tile.Position), false, true);
            }
            string entName = bookend + entity.BaseType + space + distance + space + direction + bookend;
            
            string padding = "";
            for (int x = 0; x < entName.Count(); x++)
                padding = padding + " ";

            //messagesToPrint.Add(new CharacterString(padding, frontCol, backCol));
            messagesToPrint.Add(new CharacterString(entName, frontCol, backCol));
            //messagesToPrint.Add(new CharacterString(padding, frontCol, backCol));
        }

        

        private static void AppendBreakline(List<CharacterString> messagesToPrint)
        {
           // messagesToPrint.Add(new CharacterString("-------------", TermColor.Gray, TermColor.Black));
            messagesToPrint.Add(new CharacterString("", TermColor.Gray, TermColor.Black));
        }
    }
}
