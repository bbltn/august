﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    class TileWindow : Window
    {
        public TileWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(Model model)
        {
            base.Update(model);

            var tile = model.Scene.getPlayer.tile;
            List<CharacterString> messagesToPrint = new List<CharacterString>();

            messagesToPrint.Add(new CharacterString(tile.Position.ToString()));
            if (tile.hasFeature) messagesToPrint.Add(new CharacterString("Feature: " + tile.Feature.Name));
            if (tile.hasEntity) messagesToPrint.Add(new CharacterString("Entity: " + tile.Entity.BaseType));

            for (int x = 0; x < messagesToPrint.Count; x++)
                frame.Write(new Vec(0, x), messagesToPrint[x]);

        }
    }
}
