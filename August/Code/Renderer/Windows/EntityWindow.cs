﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    class EntityWindow : Window
    {
        public EntityWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(Model model)
        {
            base.Update(model);

            var entity = model.Scene.getPlayer;
            List<CharacterString> messagesToPrint = new List<CharacterString>();

            AppendBreakline(messagesToPrint);
            AppendHealth(entity, messagesToPrint);
            AppendBreakline(messagesToPrint);

            AppendStance(entity, messagesToPrint);
            AppendBreakline(messagesToPrint);

            AppendEntityKnowledge(entity, messagesToPrint);
            AppendBreakline(messagesToPrint);

            AppendAlarm(entity, messagesToPrint);
            AppendBreakline(messagesToPrint);

            AppendSkills(entity, messagesToPrint);
            AppendBreakline(messagesToPrint);

            //AppendNextMove(entity, messagesToPrint);
            
            for (int x = 0; x < messagesToPrint.Count; x++)
                frame.Write(new Vec(0, x), messagesToPrint[x]);
            
        }

        private static void AppendStance(IEntity entity, List<CharacterString> messagesToPrint)
        {
            messagesToPrint.Add(new CharacterString("Stance: " + entity.stance.current.ToString(), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
        }

        private static void AppendNextMove(IEntity entity, List<CharacterString> messagesToPrint)
        {
            if (entity.curEvent == null)
                messagesToPrint.Add(new CharacterString("Event: null", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            else
                messagesToPrint.Add(new CharacterString("Event: " + entity.curEvent.ToString(), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
        }

        private static void AppendHealth(IEntity entity, List<CharacterString> messagesToPrint)
        {
            string healthString = "HP: " + entity.health.curHP.ToString() + "/" + entity.health.maxHP.ToString();
            if ((entity.health.maxHP - entity.health.curHP) > entity.health.curHP)
                messagesToPrint.Add(new CharacterString(healthString, CommonGraphics.WINDOW_TEXT_BOLD.ForeColor, TermColor.Red));
            else if (entity.health.maxHP == entity.health.curHP)
                messagesToPrint.Add(new CharacterString(healthString, CommonGraphics.WINDOW_TEXT_BOLD.ForeColor, TermColor.DarkGreen));
            else
                messagesToPrint.Add(new CharacterString(healthString, CommonGraphics.WINDOW_TEXT_BOLD.ForeColor, TermColor.Orange));

            string knockoutString = "KO: " + entity.health.curKO.ToString() + "/" + entity.health.maxKO.ToString();
            if ((entity.health.maxKO - entity.health.curKO) > entity.health.curKO)
                messagesToPrint.Add(new CharacterString(knockoutString, CommonGraphics.WINDOW_TEXT_BOLD.ForeColor, TermColor.Red));
            else if (entity.health.maxKO == entity.health.curKO)
                messagesToPrint.Add(new CharacterString(knockoutString, CommonGraphics.WINDOW_TEXT_BOLD.ForeColor, TermColor.DarkGreen));
            else
                messagesToPrint.Add(new CharacterString(knockoutString, CommonGraphics.WINDOW_TEXT_BOLD.ForeColor, TermColor.Orange));
        }

        private static void AppendAlarm(IEntity entity, List<CharacterString> messagesToPrint)
        {
            messagesToPrint.Add(new CharacterString("Alarm: " + entity.knowledge.Alarm.ToString(), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
        }

        private static void AppendEntityKnowledge(IEntity entity, List<CharacterString> messagesToPrint)
        {
            messagesToPrint.Add(new CharacterString("Friends: " + entity.knowledge.Friends.Count.ToString(), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Enemies: " + entity.knowledge.Enemies.Count.ToString(), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
            messagesToPrint.Add(new CharacterString("Adjacent: " + entity.knowledge.GetAdjacentEnemies().Count.ToString(), CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
        }

        private static void AppendBreakline(List<CharacterString> messagesToPrint)
        {
            messagesToPrint.Add(new CharacterString("-------------", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
        }

        private static void AppendSkills(IEntity entity, List<CharacterString> messagesToPrint)
        {
            for (int x = 0; x < entity.skills.Count; x++)
            {
                messagesToPrint.Add(new CharacterString(entity.skills.CheckName(x) + ": ", CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));

                TermColor backCol = CommonGraphics.WINDOW_TEXT_STANDARD.BackColor;
                TermColor foreCol = CommonGraphics.WINDOW_TEXT_STANDARD.ForeColor;
                switch (entity.skills.CheckAptitude(x))
                {
                    case AptitudeLevel.Expert:
                        foreCol = CommonGraphics.WINDOW_TEXT_BOLD.ForeColor;
                        backCol = TermColor.DarkGreen;
                        break;

                    case AptitudeLevel.Trained:
                        foreCol = CommonGraphics.WINDOW_TEXT_BOLD.ForeColor;
                        backCol = TermColor.DarkYellow;
                        break;

                    default:
                        backCol = CommonGraphics.WINDOW_TEXT_STANDARD.BackColor;
                        break;
                }
                messagesToPrint.Add(new CharacterString("  " + entity.skills.CheckAptitude(x).ToString(), foreCol, backCol));
                messagesToPrint.Add(new CharacterString(" ", foreCol, CommonGraphics.WINDOW_TEXT_STANDARD.BackColor));
                //messagesToPrint.Add(new CharacterString(" ", TermColor.White, backCol) + new CharacterString("  " + entity.skills.CheckAptitude(x).ToString()));
            }
        }
    }
}
