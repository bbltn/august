﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    public class GameWindow : Window
    {
        private static Character playerGraphic = new Character(glyph: Glyph.Face, foreColor: TermColor.White);

        bool debugPathfinding = false;
        bool debugModelComponents = false;
        bool debugFPS = false;

        public GameWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(Model model)
        {
            frame.Fill(new Character(' '));

            Vec drawPosition = new Vec(0,0);
            Tile tile;

            for (int x = 0; x < Size.X; x++)
            {
                for (int y = 0; y < Size.Y; y++)
                {
                    drawPosition.X = x;
                    drawPosition.Y = y;

                    tile = model.Scene.GetTile(WindowSpaceToSceneSpace(drawPosition, model.Scene));

                    frame.Set(drawPosition, GetHighestPriorityGraphic(tile));
                }
            }

            if (debugFPS)
            {
                frame.Write(Vec.Zero, new CharacterString("debugFPS enabled", TermColor.White, TermColor.Black));

                var fps = model.FPS;
                TermColor foreCol = TermColor.Red;
                if (fps >= 60)
                    foreCol = TermColor.Green;
                else if (fps >= 30)
                    foreCol = TermColor.Yellow;


                frame.Write(new Vec(0, 1), new CharacterString("FPS: " + fps.ToString(), foreCol, TermColor.Black));
            }

            if (debugModelComponents)
            {
                frame.Write(Vec.Zero, new CharacterString("debugModelComponents enabled", TermColor.White, TermColor.Black));
                frame.Write(new Vec(0, 1), new CharacterString("Commands: " + model.Commands.Count.ToString(), TermColor.Red, TermColor.Black));
                frame.Write(new Vec(0, 2), new CharacterString("Events: " + model.Events.Count.ToString(), TermColor.Red, TermColor.Black));
            }

            if (debugPathfinding)
            {
                IEntity target = model.Scene.getPlayer;

                var targetVec = Vec.Zero;

                var path = Navigation.CreatePath(model, target.tile.Position, targetVec);
                
                foreach (var vec in path)
                {
                    frame.Set(SceneSpaceToWindowSpace(vec, model.Scene), new Character('+', TermColor.Pink, TermColor.DarkGold));
                }

                
                frame.Write(Vec.Zero, new CharacterString("debugPathfinding enabled", TermColor.White, TermColor.Black));
                if (path[path.Count - 1] != targetVec)
                {
                    frame.Write(new Vec(0, 1), new CharacterString("ERROR", TermColor.Red, TermColor.Black));
                    frame.Write(new Vec(0, 2), new CharacterString(path[path.Count - 1].ToString(), TermColor.LightGray, TermColor.Black));
                }
                 
            }
        }

        private static Character GetHighestPriorityGraphic(Tile tile)
        {
            if (tile == null)
                return new Character('!', TermColor.LightRed);
            
            if (tile.hasEntity)
                return tile.Entity.Graphic;

            if (tile.hasFeature)
                return tile.Feature.Graphic;

            return tile.Graphic;
        }

        private Vec SceneSpaceToWindowSpace(Vec value, IScene scene)
        {
            return value - scene.CameraPosition + (Size / 2);
        }

        private Vec WindowSpaceToSceneSpace(Vec value, IScene scene)
        {
            return value + scene.CameraPosition - (Size / 2);
        }
    }
}
