﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    class MessageLogWindow : Window
    {
        public bool NewestAtTop = false;

        public MessageLogWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(Model model)
        {
            base.Update(model);

            if (NewestAtTop)
                WriteMessagesTopToBottom(model);
            else
                WriteMessagesBottomToTop(model);
        }

        private void WriteMessagesTopToBottom(Model model)
        {
            for (int iter = 0; iter < model.Messages.Count; iter++)
            {
                frame.Write(new Vec(0, iter), model.Messages.Get(model.Messages.Count - iter));
            }
        }

        private void WriteMessagesBottomToTop(Model model)
        {
            for (int iter = 0; iter < model.Messages.Count; iter++)
            {
                frame.Write(new Vec(0, iter), model.Messages.Get(model.Messages.Count + iter + 1 + (model.Messages.Count - Size.Y)));
            }
        }       
    }
}
