﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;
using Malison.WinForms;

namespace August
{
    public class Renderer
    {
        private FrameGenerator frameGenerator;
        private ITerminal Terminal;

        public Renderer(ITerminal terminal, Vec size)
        {
            Terminal = terminal;
            frameGenerator = new FrameGenerator(size);
        }


        public void Update(Model model)
        {
            frameGenerator.Update(model);
            RenderChanges();
        }


        private void RenderChanges()
        {
            List<Vec> changedPoints = frameGenerator.GetChangedPoints();

            foreach (Vec changedPoint in changedPoints)
            {
                Terminal[changedPoint.X, changedPoint.Y].Write(frameGenerator.CharAtPosition(changedPoint));
            }
        }        
    }
}
