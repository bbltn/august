﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace August
{
    public interface IWindow
    {
        Vec Origin { get; }
        Vec Size { get; }
        Frame frame { get; }
        void Update(Model model);
        bool isVisible { get; set; }
        bool isTransparent { get; set; }
    }

    public class Window : IWindow
    {
        public bool isVisible { get; set; }
        public bool isTransparent { get; set; }
        public Vec Origin { get; private set; }
        public Frame frame { get; private set; }
        public Vec Size 
        { 
            get 
            { 
                return frame.Size; 
            } 
        }

        public Window(Vec origin, Vec size, bool visible = true, bool transparent = false)
        {
            Origin = origin;
            frame = new Frame(size);
            isVisible = visible;
            isTransparent = transparent;
        }

        public virtual void Update(Model model)
        {
            //frame.Fill(new Character(' ', TermColor.White, TermColor.Black));
            frame.Fill(CommonGraphics.WINDOW_VOID);
        }
    }
}
