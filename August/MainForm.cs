﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Bramble.Core;
using Malison.Core;
using Malison.WinForms;

namespace August
{   
    public partial class MainForm : TerminalForm
    {
        const int DEFAULT_RENDER_X = 160;
        const int DEFAULT_RENDER_Y = 58;
        static Vec getDefaultRenderSize { get { return new Vec(DEFAULT_RENDER_X, DEFAULT_RENDER_Y); } }
        const float VERSION = 0.46f;

        private System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        private const int FPS_TARGET = 60;
        

        Renderer renderer;
        Model model;
        public IInputProcessor Input { get; private set; }


        public MainForm()
        {
            // required method for malison, seems to configure form settings
            InitializeComponent(); 
            
            AddHandlers();
            CreateTerminalAndRenderer();
            CreateModel();
            //LoadData()
            ConfigureFormSettings();
            CreateInput();

            stopwatch.Reset();
            stopwatch.Start();
        }

        private void CreateInput()
        {
            Input = new InputProcessor();
        }

        private void AddHandlers()
        {
            Application.Idle += HandleApplicationIdle;
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(HandleKeyDown);
        }

        private void CreateTerminalAndRenderer()
        {
            Terminal = new Terminal(DEFAULT_RENDER_X, DEFAULT_RENDER_Y) as ITerminal;
            renderer = new Renderer(Terminal, getDefaultRenderSize);
        }

        private void CreateModel()
        {
            model = new Model();
        }

        private void ConfigureFormSettings()
        {
            this.Text = "August v" + VERSION.ToString();
        }

        private void HandleKeyDown(object sender, KeyPressEventArgs e)
        {
            Input.Register(e.KeyChar);
            RenderToScreen();
        }

        private void HandleApplicationIdle(object sender, EventArgs e)
        {
            while (IsApplicationIdle())
            {
                UpdateModel();
                RenderToScreen();
            }
        }


        private void UpdateModel() 
        {
            model.Update();
            if (model.NeedsCommand)
                model.AddCommand(Input.PullCommand());
        }


        private void RenderToScreen()
        {

            //model.Messages.Add(stopwatch.ElapsedMilliseconds.ToString());

            if (stopwatch.ElapsedMilliseconds < (1000 / FPS_TARGET))
                return;
            else
            {
                model.FPS = 1000 / stopwatch.ElapsedMilliseconds;
                stopwatch.Reset();
                stopwatch.Start();
            }

            renderer.Update(model);
        }


        // returns true if there are no events in the pump
        bool IsApplicationIdle()
        {
            NativeMessage result;
            return PeekMessage(out result, IntPtr.Zero, (uint)0, (uint)0, (uint)0) == 0;
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NativeMessage
        {
            public IntPtr Handle;
            public uint Message;
            public IntPtr WParameter;
            public IntPtr LParameter;
            public uint Time;
            public Point Location;
        }

        [DllImport("user32.dll")]
        public static extern int PeekMessage(out NativeMessage message, IntPtr window, uint filterMin, uint filterMax, uint remove);


        
    }
}
